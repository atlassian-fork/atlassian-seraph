<?xml version="1.0" encoding="UTF-8"?>
<document>
    <properties>
        <title>Security Services</title>
    </properties>
    <section name="Security Services">
    <p>
        Security services in Seraph determine the roles required for any given request.
        There are two security services bundled with Seraph - the Path service and the WebWork service.
        <br/>
        Security services can use any meta-data associated with the incoming request, so other examples
        of services could be an IPService (to authenticate users based on IP) or a KeyService (to authenticate
        users based on their security keys).
    </p>

    <subsection name="Path Service">
    <p>
    The <b><a href="apidocs/com/atlassian/seraph/service/PathService.html">Path Service</a></b> secures particular URL paths. It allows for extremely flexible path
    lookups (ie /admin/*, /admin/Setup*, /admin/Setup*Foo etc) and is configured via it's own XML
    configuration file, named <b>seraph-paths.xml</b>.
    <br/>
    The Path service is configured in <b>security-config.xml</b> as follows:
<source>
&lt;service class="com.atlassian.seraph.service.PathService"&gt;
    &lt;init-param&gt;
        &lt;param-name&gt;config.file&lt;/param-name&gt;
        &lt;param-value&gt;/seraph-paths.xml&lt;/param-value&gt;
    &lt;/init-param&gt;
&lt;/service&gt;
</source>
    The <b>seraph-paths.xml</b> file contains the secured paths and the roles required for each. Here is a sample:
<source>
&lt;seraph-paths&gt;
    &lt;!-- You can configure any number of path elements --&gt;
    &lt;path name="admin"&gt;
        &lt;url-pattern&gt;/admin/*&lt;/url-pattern&gt;
		&lt;role-name&gt;myapp-administrators, myapp-owners&lt;/role-name&gt;
    &lt;/path&gt;
&lt;/seraph-paths&gt;
</source>
    </p>
    </subsection>
    <subsection name="WebWork Service">
        <p>
        The <b><a href="apidocs/com/atlassian/seraph/service/WebworkService.html">WebWork Service</a></b> secures WebWork 1 actions. You must use actions.xml to configure your
        actions, and then just add a "roles-required" attribute to each action or command element.
        <br/>
        Here is a snippet of <b>actions.xml</b> showing the roles-required attribute:

<source>
&lt;action name="project.AddProject" roles-required="admin"&gt;
    &lt;view name="input"&gt;/secure/admin/views/addproject.jsp&lt;/view&gt;
&lt;/action&gt;
</source>
        </p>
     </subsection>
  </section>
</document>