package com.atlassian.seraph.config;

import com.atlassian.seraph.auth.MockAuthenticator;
import com.atlassian.seraph.auth.MockRoleMapper;
import com.atlassian.seraph.auth.StubAuthenticator;
import com.atlassian.seraph.controller.NullSecurityController;
import com.atlassian.seraph.interceptor.LoginInterceptor;
import com.atlassian.seraph.interceptor.LogoutInterceptor;
import com.atlassian.seraph.service.PathService;
import junit.framework.TestCase;
import mock.MockLoginInterceptor;

// DISABLED temporarily, since this test fails depending on which order the
// test is run in:

// [junit] Will fork only once
//     [junit] Running mutiple tests
//    [junit] Running com.atlassian.seraph.logout.TestLogoutServlet
//    [junit] Tests run: 2, Failures: 0, Errors: 0, Time elapsed: 1.009 sec
//    [junit] Running com.atlassian.seraph.util.TestRedirectUtils
//    [junit] Tests run: 4, Failures: 0, Errors: 0, Time elapsed: 0.037 sec
//    [junit] Running com.atlassian.seraph.util.TestCookieUtils
//    [junit] Tests run: 1, Failures: 0, Errors: 0, Time elapsed: 0.108 sec
//    [junit] Running com.atlassian.seraph.auth.TestDefaultAuthenticator
//    [junit] Tests run: 1, Failures: 0, Errors: 0, Time elapsed: 0.324 sec
//    [junit] Running com.atlassian.seraph.controller.TestNullSecurityController
//    [junit] Tests run: 1, Failures: 0, Errors: 0, Time elapsed: 0.013 sec
//    [junit] Running com.atlassian.seraph.config.TestSecurityConfig
//    [junit] Tests run: 1, Failures: 1, Errors: 0, Time elapsed: 0.079 sec
//    [junit] TEST com.atlassian.seraph.config.TestSecurityConfig FAILED

public class _TestSecurityConfig extends TestCase
{
    public void testSecurityConfig() throws ConfigurationException
    {
        SecurityConfigFactory.setSecurityConfig(null);
        SecurityConfig config = SecurityConfigFactory.getInstance("test-seraph-config.xml");
        assertEquals(StubAuthenticator.class, config.getAuthenticator().getClass());
        assertEquals(MockRoleMapper.class, config.getRoleMapper().getClass());
        assertTrue(config.getController() instanceof NullSecurityController);
        assertEquals(1, config.getServices().size());
        assertTrue(config.getServices().get(0) instanceof PathService);

        assertEquals("/login.action", config.getLoginURL());
        assertEquals(100, config.getAutoLoginCookieAge());
        assertFalse(config.isInsecureCookie());
        assertEquals("test_security_originalurl", config.getOriginalURLKey());
        assertEquals(0, config.getInterceptors(LogoutInterceptor.class).size());
        assertEquals(1, config.getInterceptors(LoginInterceptor.class).size());
        assertTrue(config.getInterceptors(LoginInterceptor.class).get(0) instanceof MockLoginInterceptor);
    }

    public void testSecurityConfigWithoutInsecureFlag() throws ConfigurationException
    {
        SecurityConfigFactory.setSecurityConfig(null);
        SecurityConfig config = SecurityConfigFactory.getInstance("test-seraph-config-without-insecure-flag.xml");
        assertEquals(MockAuthenticator.class, config.getAuthenticator().getClass());
        assertEquals(MockRoleMapper.class, config.getRoleMapper().getClass());
        assertTrue(config.getController() instanceof NullSecurityController);
        assertEquals(1, config.getServices().size());
        assertTrue(config.getServices().get(0) instanceof PathService);

        assertEquals("/login.action", config.getLoginURL());
        assertEquals(100, config.getAutoLoginCookieAge());
        assertFalse(config.isInsecureCookie());
        assertEquals("test_security_originalurl", config.getOriginalURLKey());
        assertEquals(0, config.getInterceptors(LogoutInterceptor.class).size());
        assertEquals(1, config.getInterceptors(LoginInterceptor.class).size());
        assertTrue(config.getInterceptors(LoginInterceptor.class).get(0) instanceof MockLoginInterceptor);
    }

    public void testSecurityConfigWithInsecureFlag() throws ConfigurationException
    {
        SecurityConfigFactory.setSecurityConfig(null);
        SecurityConfig config = SecurityConfigFactory.getInstance("test-seraph-config-with-insecure-flag.xml");
        assertEquals(MockAuthenticator.class, config.getAuthenticator().getClass());
        assertEquals(MockRoleMapper.class, config.getRoleMapper().getClass());
        assertTrue(config.getController() instanceof NullSecurityController);
        assertEquals(1, config.getServices().size());
        assertTrue(config.getServices().get(0) instanceof PathService);

        assertEquals("/login.action", config.getLoginURL());
        assertEquals(100, config.getAutoLoginCookieAge());
        assertTrue(config.isInsecureCookie());
        assertEquals("test_security_originalurl", config.getOriginalURLKey());
        assertEquals(0, config.getInterceptors(LogoutInterceptor.class).size());
        assertEquals(1, config.getInterceptors(LoginInterceptor.class).size());
        assertTrue(config.getInterceptors(LoginInterceptor.class).get(0) instanceof MockLoginInterceptor);
    }

    public void testPluggableLoginUrlStrategy() throws ConfigurationException
    {
        SecurityConfigFactory.setSecurityConfig(null);
        SecurityConfig config = SecurityConfigFactory.getInstance("test-seraph-config-with-login-strategy.xml");

        assertEquals("/inserted/login.action", config.getLoginURL());
        assertEquals("/inserted/linklogin.action", config.getLinkLoginURL());
        assertEquals("/inserted/logout.action", config.getLogoutURL());
    }

    public void testCustomCookiePath() throws ConfigurationException
    {
        SecurityConfigFactory.setSecurityConfig(null);
        SecurityConfig config = SecurityConfigFactory.getInstance("test-seraph-config-with-custom-cookie-path.xml");

        assertEquals("/foo", config.getLoginCookiePath());
        assertEquals("some-key", config.getLoginCookieKey());
    }
}
