package com.atlassian.seraph.util;

import junit.framework.TestCase;

import static com.atlassian.seraph.util.ServerInformationParser.ServerInformation;

public class TestServerInformationParser extends TestCase
{
    public void testNameVersionWithoutOtherInformation()
    {
        ServerInformation info = ServerInformationParser.parse("Apache Tomcat/6.0.20");
        assertEquals("Apache Tomcat", info.getName());
        assertEquals("6.0.20", info.getVersion());
        assertEquals(null, info.getOtherInformation());
        assertEquals(true, info.isApacheTomcat());
    }

    public void testNameVersionWithOtherInformation()
    {
        ServerInformation info = ServerInformationParser.parse("Apache Tomcat/6.0.20 (JDK 1.6.0)");
        assertEquals("Apache Tomcat", info.getName());
        assertEquals("6.0.20", info.getVersion());
        assertEquals("JDK 1.6.0", info.getOtherInformation());
        assertEquals(true, info.isApacheTomcat());
    }
    public void testNonApacheTomcatServer()
    {
        ServerInformation info = ServerInformationParser.parse("Other Application Server/1.2.3");
        assertEquals(false, info.isApacheTomcat());
    }

    public void testMalformedStringFails()
    {
        try
        {
            ServerInformationParser.parse("malformed server information string");
            fail("Exception should have been thrown upon detecting illegal argument");
        }
        catch (IllegalArgumentException e)
        {
            assertTrue(true);
        }
    }
}
