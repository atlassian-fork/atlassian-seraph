package com.atlassian.seraph.filter;

import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.servlet.ServletContext;

/**
*/
class MockSession implements HttpSession
{
    private final Map<String, Object> attributeMap = new HashMap<String, Object>();

    @Override
    public long getCreationTime()
    {
        return 0;
    }

    @Override
    public String getId()
    {
        return null;
    }

    @Override
    public long getLastAccessedTime()
    {
        return 0;
    }

    @Override
    public ServletContext getServletContext()
    {
        return null;
    }

    @Override
    public void setMaxInactiveInterval(final int i)
    {}

    @Override
    public int getMaxInactiveInterval()
    {
        return 0;
    }

    @Override
    public HttpSessionContext getSessionContext()
    {
        return null;
    }

    @Override
    public Object getAttribute(final String s)
    {
        return attributeMap.get(s);
    }

    @Override
    public Object getValue(final String s)
    {
        return getAttribute(s);
    }

    @Override
    public Enumeration<String> getAttributeNames()
    {
        //Converting from iterator to enumeration
        class MyEnumeration implements Enumeration<String> {
            private Iterator<String> it;
            public MyEnumeration(Iterator<String> it) {this.it = it;}
            @Override
            public boolean hasMoreElements()          {return it.hasNext();}
            @Override
            public String nextElement()               {return it.next();}
        }
        return (new MyEnumeration(attributeMap.keySet().iterator()));
    }

    @Override
    public String[] getValueNames()
    {
        return new String[0];
    }

    @Override
    public void setAttribute(final String s, final Object o)
    {
        attributeMap.put(s, o);
    }

    @Override
    public void putValue(final String s, final Object o)
    {
        setAttribute(s,o);
    }

    @Override
    public void removeAttribute(final String s)
    {
        attributeMap.remove(s);
    }

    @Override
    public void removeValue(final String s)
    {
        removeAttribute(s);
    }

    @Override
    public void invalidate()
    {
        attributeMap.clear();
    }

    @Override
    public boolean isNew()
    {
        return false;
    }
}
