package com.atlassian.seraph.auth;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Mock implementation of HttpSession with map backing and nothing much else.
 */
class MockSession implements HttpSession
{

    private final Map<String, Object> attributes = new HashMap<String, Object>();
    private boolean isNew;
    private long invalidateCount = 0;

    MockSession(boolean isNew)
    {
        this.isNew = isNew;
    }

    @Override
    public Object getAttribute(final String name)
    {
        return attributes.get(name);
    }

    @Override
    public long getCreationTime()
    {
        return 0;
    }

    @Override
    public String getId()
    {
        return String.valueOf(System.identityHashCode(this)) + "_" + invalidateCount;
    }

    @Override
    public long getLastAccessedTime()
    {
        return 0;
    }

    @Override
    public ServletContext getServletContext()
    {
        return null;
    }

    @Override
    public void setMaxInactiveInterval(final int interval)
    {
    }

    @Override
    public int getMaxInactiveInterval()
    {
        return 0;
    }

    @Override
    public HttpSessionContext getSessionContext()
    {
        return null;
    }

    @Override
    public Object getValue(final String name)
    {
        return attributes.get(name);
    }

    @Override
    public Enumeration getAttributeNames()
    {
        final Iterator attributeKeys = attributes.keySet().iterator();
        return new Enumeration()
        {
            @Override
            public boolean hasMoreElements()
            {
                return attributeKeys.hasNext();
            }

            @Override
            public Object nextElement()
            {
                return attributeKeys.next();
            }

        };
    }

    @Override
    public String[] getValueNames()
    {
        return (String[]) attributes.keySet().toArray();

    }

    @Override
    public void setAttribute(final String name, final Object value)
    {
        attributes.put(name, value);
    }

    @Override
    public void putValue(final String name, final Object value)
    {
        attributes.put(name, value);
    }

    @Override
    public void removeAttribute(final String name)
    {
        attributes.remove(name);
    }

    @Override
    public void removeValue(final String name)
    {
        attributes.remove(name);
    }

    @Override
    public boolean isNew()
    {
        return isNew;
    }

    @Override
    public void invalidate()
    {
        isNew = true;
        invalidateCount++;
        attributes.clear();
    }

    public long getInvalidateCount()
    {
        return invalidateCount;
    }
}
