package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;

import java.security.Principal;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * A Mock RoleMapper used only in tests for loading config.
 */
public class MockRoleMapper implements RoleMapper
{
    @Override
    public boolean hasRole(final Principal user, final HttpServletRequest request, final String role)
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public boolean canLogin(final Principal user, final HttpServletRequest request)
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        // do nothing
    }
}
