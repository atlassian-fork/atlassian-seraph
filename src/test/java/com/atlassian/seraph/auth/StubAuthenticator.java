package com.atlassian.seraph.auth;

import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;
import com.atlassian.seraph.service.rememberme.RememberMeService;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Sub-class of the DefaultAuthenticator that has some back doors to allow for testing.
 */
public class StubAuthenticator extends DefaultAuthenticator
{
    private final Map<String, Principal> users = new HashMap<String, Principal>();
    private ElevatedSecurityGuard elevatedSecurityGuard;
    private RememberMeService rememberMeService;
    private boolean desiredLoginAnswer = true;

    @Override
    public boolean login(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password, final boolean cookie)
    {
        return desiredLoginAnswer;
    }

    @Override
    protected boolean authoriseUserAndEstablishSession(final HttpServletRequest request, final HttpServletResponse response, final Principal principal)
    {
        return desiredLoginAnswer;
    }

    @Override
    protected Principal getUser(final String username)
    {
        return users.get(username);
    }

    @Override
    protected boolean authenticate(final Principal user, final String password)
    {
        throw new UnsupportedOperationException("Not implemented yet.");
    }

    void addUser(final String username, final Principal user)
    {
        users.put(username, user);
    }

    @Override
    protected ElevatedSecurityGuard getElevatedSecurityGuard()
    {
        if (elevatedSecurityGuard == null)
        {
            return super.getElevatedSecurityGuard();
        }
        return elevatedSecurityGuard;
    }

    @Override
    protected RememberMeService getRememberMeService()
    {
        if (rememberMeService == null)
        {
            return super.getRememberMeService();
        }
        return rememberMeService;
    }

    protected void setElevatedSecurityGuard(ElevatedSecurityGuard elevatedSecurityGuard)
    {
        this.elevatedSecurityGuard = elevatedSecurityGuard;
    }

    protected void setRememberMeService(RememberMeService rememberMeService)
    {
        this.rememberMeService = rememberMeService;
    }

    void setDesiredLoginAnswer(boolean desiredLoginAnswer)
    {
        this.desiredLoginAnswer = desiredLoginAnswer;
    }
}
