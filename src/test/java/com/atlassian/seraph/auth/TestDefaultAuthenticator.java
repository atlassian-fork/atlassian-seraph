package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.ConfigurationException;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.config.SecurityConfigImpl;
import com.atlassian.seraph.elevatedsecurity.CountingElevatedSecurityGuard;
import com.atlassian.seraph.interceptor.LogoutInterceptor;
import com.atlassian.seraph.service.rememberme.RememberMeService;
import com.atlassian.seraph.util.SecurityUtils;
import junit.framework.TestCase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * This test uses a mix of shitty old Mock objects and the new Mockito framework.
 * <p>
 * Like most of the olden day Atlassian code, this test is an abomination of hacks and forced calls.
 * <p>
 * I tried but failed to get it into shape and hence its still an abomination of hacks and forced calls.
 */
public class TestDefaultAuthenticator extends TestCase
{
    private HttpServletRequest request;
    private HttpServletResponse response;
    private SecurityConfigImpl config;
    private StubAuthenticator authenticator;
    private HttpSession session;
    private Principal user;
    private RememberMeService rememberMeService;
    private static final String TEST_USERNAME = "üsernåme";
    private static final String TEST_PASSWORD = "pässword";

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        session = mock(HttpSession.class);
        SecurityConfigFactory.setSecurityConfig(null);
        config = (SecurityConfigImpl) SecurityConfigFactory.getInstance("test-seraph-config.xml");
        authenticator = (StubAuthenticator) config.getAuthenticator();
        user = new Principal()
        {
            @Override
            public String getName()
            {
                return "Test User";
            }
        };

        rememberMeService = mock(RememberMeService.class);
        authenticator.setRememberMeService(rememberMeService);

        authenticator.addUser(user.getName(), user);
    }

    public void testLogout() throws ConfigurationException, AuthenticatorException
    {
        // mocks
        final LogoutInterceptor logoutInterceptor = mock(LogoutInterceptor.class);

        // expectations
        when(request.getSession()).thenReturn(session);
        mockLoginReason();

        config.addInterceptor(logoutInterceptor);

        // do test!
        authenticator.logout(request, response);

        // verify everything
        verify(session).setAttribute(DefaultAuthenticator.LOGGED_IN_KEY, null);
        verify(session).setAttribute(DefaultAuthenticator.LOGGED_OUT_KEY, true);
        verify(logoutInterceptor).beforeLogout(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verify(logoutInterceptor).afterLogout(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verifyLoginReason(LoginReason.OUT);
    }

    public void testGetUserChecksSessionFirst()
    {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);
        when(session.getAttribute(DefaultAuthenticator.LOGGED_OUT_KEY)).thenReturn(null);
        when(session.getAttribute(DefaultAuthenticator.LOGGED_IN_KEY)).thenReturn(user);

        mockLoginReason();

        final Principal result = authenticator.getUser(request, response);
        assertEquals(user, result);

        verify(session).setAttribute(DefaultAuthenticator.LOGGED_IN_KEY, user);
        verify(session).setAttribute(DefaultAuthenticator.LOGGED_OUT_KEY, null);
        verifyLoginReason(LoginReason.OK);
    }

    private StubAuthenticator setupRememberMeCookieState()
    {
        when(request.getSession(false)).thenReturn(null); // no session during initial check
        when(request.getSession()).thenReturn(session); // session "created" when cookie login() gets it

        mockLoginReason();

        when(session.getAttribute(DefaultAuthenticator.LOGGED_OUT_KEY)).thenReturn(null);
        when(session.getAttribute(DefaultAuthenticator.LOGGED_IN_KEY)).thenReturn(user);
        return authenticator;
    }

    public void testGetUserChecksCookieIfNoSession()
    {
        setupRememberMeCookieState();

        when(rememberMeService.getRememberMeCookieAuthenticatedUsername(request, response)).thenReturn(user.getName());

        final Principal result = authenticator.getUser(request, response);
        assertEquals(user, result);

        verifyLoginReason(LoginReason.OK);
    }

    public void testGetUserChecksCookieIfNoSession_ButFailedAuthorisation()
    {
        authenticator.setDesiredLoginAnswer(false);
        setupRememberMeCookieState();

        // basic auth checks happen now if we fail
        when(request.getQueryString()).thenReturn("p=v");
        when(request.getHeader("Authorization")).thenReturn(null);

        when(rememberMeService.getRememberMeCookieAuthenticatedUsername(request, response)).thenReturn(user.getName());

        final Principal result = authenticator.getUser(request, response);
        assertNull(result);

        verify(request).getQueryString();
        verify(request).getHeader("Authorization");
        verifyLoginReason(LoginReason.AUTHORISATION_FAILED);
    }

    public void testGetUserChecksCookieIfSessionWithNoUser()
    {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute(DefaultAuthenticator.LOGGED_OUT_KEY)).thenReturn(null);
        when(session.getAttribute(DefaultAuthenticator.LOGGED_IN_KEY)).thenReturn(null);
        mockLoginReason();

        when(rememberMeService.getRememberMeCookieAuthenticatedUsername(request, response)).thenReturn(user.getName());

        final Principal result = authenticator.getUser(request, response);
        assertEquals(user, result);

        verify(session).getAttribute(DefaultAuthenticator.LOGGED_OUT_KEY);
        verify(session).getAttribute(DefaultAuthenticator.LOGGED_IN_KEY);
        verifyLoginReason(LoginReason.OK);
    }

    public void testGetUserDoesNotCheckCookieIfLogoutRequest()
    {
        when(request.getSession()).thenReturn(session);
        when(request.getSession(false)).thenReturn(session);
        when(request.getAttribute(LoginReason.REQUEST_ATTR_NAME)).thenReturn(LoginReason.OUT);

        final Principal result = authenticator.getUser(request, response);
        assertEquals(null, result);

        verify(rememberMeService, times(0)).getRememberMeCookieAuthenticatedUsername(request, response);
    }

    public void testGetUserReturnsNullWithNoValidAuthentication()
    {
        when(request.getSession(false)).thenReturn(null);
        when(request.getQueryString()).thenReturn(null); // no basic auth
        when(request.getHeader("Authorization")).thenReturn(null);

        final Principal result = authenticator.getUser(request, response);
        assertEquals(null, result);

        verify(request).getQueryString();
        verify(request).getHeader("Authorization");
    }

    public void testGetUserBasicAuthRequiredButMissing()
    {
        when(request.getSession(false)).thenReturn(null);
        when(request.getCookies()).thenReturn(null);
        when(request.getQueryString()).thenReturn("os_authType=basic"); // basic auth

        // no Authorization header
        when(request.getHeader("Authorization")).thenReturn(null);

        final Principal result = authenticator.getUser(request, response);
        assertEquals(null, result);

        // we expect a 401 Authentication required
        verify(request).getHeader("Authorization");
        verify(response).setStatus(401);
        verify(response).setHeader("WWW-Authenticate", "Basic realm=\"protected-area\"");
    }

    public void testGetUserBasicAuthRequiredButMissingNoResponse()
    {
        when(request.getSession(false)).thenReturn(null);
        when(request.getCookies()).thenReturn(null);
        when(request.getQueryString()).thenReturn("os_authType=basic"); // basic auth

        // no Authorization header
        when(request.getHeader("Authorization")).thenReturn(null);

        final Principal result = authenticator.getUser(request, null);
        assertNull(result);

        // we expect a 401 Authentication required
        verify(request).getHeader("Authorization");
    }

    private StubAuthenticator setupBasicAuthHttpState()
    {
        when(request.getSession(false)).thenReturn(null);
        when(request.getCookies()).thenReturn(null);
        when(request.getQueryString()).thenReturn("os_authType=basic"); // basic auth

        mockLoginReason();

        // valid Authorization header
        when(request.getHeader("Authorization")).thenReturn(
                SecurityUtils.encodeBasicAuthorizationCredentials(TEST_USERNAME, TEST_PASSWORD));
        // add the correct user to the stub
        final StubAuthenticator stubAuthenticator = authenticator;
        stubAuthenticator.addUser(TEST_USERNAME, user);
        return stubAuthenticator;
    }

    private void verifyBasicAuthHttpState(final LoginReason loginReason)
    {
        verify(request).getHeader("Authorization");
        verifyLoginReason(loginReason);
    }

    public void testGetUserBasicAuthProvidedUsingRequestParameter()
    {
        setupBasicAuthHttpState();

        final Principal result = authenticator.getUser(request, response);
        assertEquals(user, result);

        verifyBasicAuthHttpState(LoginReason.OK);
    }

    public void testGetUserBasicAuthProvidedUsingRequestParameter_ButFailsElevatedSecurity() throws ConfigurationException, IOException
    {
        final StubAuthenticator stubAuthenticator = setupBasicAuthHttpState();

        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(false, TEST_USERNAME);
        stubAuthenticator.setElevatedSecurityGuard(securityGuard);

        final Principal result = authenticator.getUser(request, response);
        assertNull(result);

        assertEquals(0, securityGuard.getSuccessCount());
        assertEquals(1, securityGuard.getFailedCount());

        // when we fail we send this back
        verify(response).sendError(401, "Basic Authentication Failure - Reason : " + LoginReason.AUTHENTICATION_DENIED);
        verifyBasicAuthHttpState(LoginReason.AUTHENTICATION_DENIED);
    }

    public void testGetUserBasicAuthProvidedUsingRequestParameter_PassesElevatedSecurity_ButFailsLogin() throws ConfigurationException, IOException
    {
        final StubAuthenticator stubAuthenticator = setupBasicAuthHttpState();
        stubAuthenticator.setDesiredLoginAnswer(false);

        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, TEST_USERNAME);
        stubAuthenticator.setElevatedSecurityGuard(securityGuard);

        final Principal result = authenticator.getUser(request, response);
        assertNull(result);

        assertEquals(0, securityGuard.getSuccessCount());
        assertEquals(1, securityGuard.getFailedCount());

        // when we fail we send this back
        verify(response).sendError(401, "Basic Authentication Failure - Reason : " + LoginReason.AUTHENTICATED_FAILED);
        verifyBasicAuthHttpState(LoginReason.AUTHENTICATED_FAILED);
    }

    public void testGetUserBasicAuthProvidedUsingRequestParameter_PassesElevatedSecurity_ButFailsLogin_NoResponse() throws ConfigurationException, IOException
    {
        final StubAuthenticator stubAuthenticator = setupBasicAuthHttpState();
        stubAuthenticator.setDesiredLoginAnswer(false);

        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, TEST_USERNAME);
        stubAuthenticator.setElevatedSecurityGuard(securityGuard);

        final Principal result = authenticator.getUser(request, null);
        assertNull(result);

        assertEquals(0, securityGuard.getSuccessCount());
        assertEquals(1, securityGuard.getFailedCount());

        verify(request).setAttribute("com.atlassian.seraph.auth.LoginReason", LoginReason.AUTHENTICATED_FAILED);
    }

    public void testGetUserBasicAuthProvidedUsingRequestParameter_PassesElevatedSecurity_AndPassesLogin() throws ConfigurationException
    {
        final StubAuthenticator stubAuthenticator = setupBasicAuthHttpState();
        stubAuthenticator.setDesiredLoginAnswer(true);

        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, TEST_USERNAME);
        stubAuthenticator.setElevatedSecurityGuard(securityGuard);

        final Principal result = authenticator.getUser(request, response);
        assertEquals(user, result);

        assertEquals(1, securityGuard.getSuccessCount());
        assertEquals(0, securityGuard.getFailedCount());

        verifyBasicAuthHttpState(LoginReason.OK);
    }

    public void testGetUserBasicAuthProvidedUsingHeader()
    {
        final String authorizationHeader = SecurityUtils.encodeBasicAuthorizationCredentials(TEST_USERNAME, TEST_PASSWORD);

        when(request.getSession(false)).thenReturn(null);
        when(request.getCookies()).thenReturn(null);
        when(request.getQueryString()).thenReturn(null); // no basic auth
        when(request.getHeader("Authorization")).thenReturn(authorizationHeader);

        mockLoginReason();

        // add the correct user to the stub
        final StubAuthenticator stubAuthenticator = authenticator;
        stubAuthenticator.addUser(TEST_USERNAME, user);

        final CountingElevatedSecurityGuard securityGuard = new CountingElevatedSecurityGuard(true, TEST_USERNAME);
        stubAuthenticator.setElevatedSecurityGuard(securityGuard);

        final Principal result = authenticator.getUser(request, response);
        assertEquals(user, result);

        assertEquals(1, securityGuard.getSuccessCount());
        assertEquals(0, securityGuard.getFailedCount());

        verify(request, times(2)).getHeader("Authorization");
        verifyLoginReason(LoginReason.OK);
    }

    private void mockLoginReason()
    {
        when(request.getAttribute("com.atlassian.seraph.auth.LoginReason")).thenReturn(null);
    }

    private void verifyLoginReason(LoginReason loginReason)
    {
        verify(request).setAttribute("com.atlassian.seraph.auth.LoginReason", loginReason);
        verify(response).addHeader(anyString(), anyString());
    }
}
