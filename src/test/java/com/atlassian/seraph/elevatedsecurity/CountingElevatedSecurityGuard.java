package com.atlassian.seraph.elevatedsecurity;

import com.atlassian.seraph.config.SecurityConfig;
import junit.framework.Assert;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * Ths guy can help when testing that a @{link ElevatedSecurityGuard} was called
*/
public class CountingElevatedSecurityGuard implements ElevatedSecurityGuard
{
    private final boolean performCheck;
    private final String expectedUserName;
    private int failedCount = 0;
    private int successCount = 0;

    public CountingElevatedSecurityGuard(final boolean performCheck, final String expectedUserName)
    {
        this.performCheck = performCheck;
        this.expectedUserName = expectedUserName;
    }

    @Override
    public boolean performElevatedSecurityCheck(final HttpServletRequest httpServletRequest, final String userName)
    {
        Assert.assertEquals(expectedUserName, userName);
        return performCheck;
    }

    @Override
    public void onFailedLoginAttempt(final HttpServletRequest httpServletRequest, final String userName)
    {
        failedCount++;
    }

    @Override
    public void onSuccessfulLoginAttempt(final HttpServletRequest httpServletRequest, final String userName)
    {
        successCount++;
    }

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
    }

    public int getFailedCount()
    {
        return failedCount;
    }

    public int getSuccessCount()
    {
        return successCount;
    }
}
