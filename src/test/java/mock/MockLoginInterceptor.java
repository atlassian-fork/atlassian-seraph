package mock;

import com.atlassian.seraph.interceptor.LoginInterceptor;
import com.atlassian.seraph.config.SecurityConfig;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Mike
 * Date: Sep 3, 2003
 * Time: 3:03:00 PM
 * To change this template use Options | File Templates.
 */
public class MockLoginInterceptor implements LoginInterceptor
{
    @Override
    public void init(Map params, SecurityConfig config)
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public void beforeLogin(HttpServletRequest request, HttpServletResponse response, String username, String password, boolean cookieLogin)
    {
    }

    @Override
    public void afterLogin(HttpServletRequest request, HttpServletResponse response, String username, String password, boolean cookieLogin, String loginStatus)
    {
    }
}
