package com.atlassian.seraph.filter;

import com.atlassian.seraph.util.SecurityUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * This is a filter that logs the user in. It parses a standard HTTP based authentication requst and logs the user in.
 * At the moment it supports only a BASIC authentication scheme. The simple way of using it manually is to request a URL in the following form:
 * protocol://user:password@host[:port]/path
 * e.g.:
 * https://juancho:sillyPass@bamboo.developer.atlassian.com/
 * <p>
 * If authentication is successful, the user will be redirected by the filter to the URL given
 * by the session attribute at SecurityFilter.ORIGINAL_URL_KEY.
 * <p>
 * If this URL doesn't exist, it will look for a parameter 'os_destination' to use as the redirected URL instead.
 * <p>
 * If neither is found, it is assumed that the page will check the authorisation status and handle redirection itself.
 * <p>
 * From the any other filter in the request, or the servlet/JSP/action which processes the request, you can look up the
 * status of the authorisation attempt. The status is a String request attribute, with the key 'os_authstatus'.
 * <p>
 * The possible statuses are:
 * <ul>
 *  <li> LoginFilter.LOGIN_SUCCESS - the login was processed, and user was logged in
 *  <li> LoginFilter.LOGIN_FAILURE - the login was processed, the user gave a bad username or password
 *  <li> LoginFilter.LOGIN_ERROR - the login was processed, an exception occurred trying to log the user in
 *  <li> LoginFilter.LOGIN_NOATTEMPT - the login was no processed, no form parameters existed
 * </ul>
 */
public class HttpAuthFilter extends PasswordBasedLoginFilter
{
	@Override
    protected UserPasswordPair extractUserPasswordPair(HttpServletRequest request)
	{
        String auth = request.getHeader("Authorization");
		if (SecurityUtils.isBasicAuthorizationHeader(auth))
        {
            SecurityUtils.UserPassCredentials creds = SecurityUtils.decodeBasicAuthorizationCredentials(auth);
            if (!"".equals(creds.getUsername()))
            {
                return new UserPasswordPair(creds.getUsername(), creds.getPassword(), false);
            }
        }
		return null;
	}
}
