package com.atlassian.seraph.filter;

import com.atlassian.seraph.RequestParameterConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * This is a filter that logs the user in. It works a little like J2EE form-based seraph, except it looks for the
 * parameters 'os_username' and 'os_password' instead of j_username and j_password.
 * <p>
 * Since 3.1.0 this filter does not let you log in through values in the URL "query string" by default
 * because your password will then end up in HTTP Request logs.
 * Host Applications can override the default or even expose a flag to users to set the required behaviour
 * if required for legacy situations.
 * <p>
 * The form post/get action should be the URL of the login servlet/JSP/action - given by SecurityFilter.LOGIN_URL.
 * <p>
 * If the parameters exist and authentication is successful, the user will be redirected by the filter to the URL given
 * by the session attribute at SecurityFilter.ORIGINAL_URL_KEY.
 * <p>
 * If this URL doesn't exist, it will look for a parameter 'os_destination' to use as the redirected URL instead.
 * <p>
 * If neither is found, it is assumed that the page will check the authorisation status and handle redirection itself.
 * <p>
 * From the any other filter in the request, or the servlet/JSP/action which processes the request, you can look up the
 * status of the authorisation attempt. The status is a String request attribute, with the key 'os_authstatus'.
 * <p>
 * The possible statuses are:
 * <ul>
 *  <li> LoginFilter.LOGIN_SUCCESS - the login was processed, and user was logged in
 *  <li> LoginFilter.LOGIN_FAILURE - the login was processed, the user gave a bad username or password
 *  <li> LoginFilter.LOGIN_ERROR - the login was processed, an exception occurred trying to log the user in
 *  <li> LoginFilter.LOGIN_NOATTEMPT - the login was no processed, no form parameters existed
 * </ul>
 */
public class LoginFilter extends PasswordBasedLoginFilter
{
    private static final Logger log = LoggerFactory.getLogger(LoginFilter.class);
    private static final String ALLOW_URL_PARAMETER_VALUE_PARAMETER_NAME = "allowUrlParameterValue";
    private static final String DISABLE_LOGGING_DEPRECATION_URL_PARAMETER_VALUE_PARAMETER_NAME =
            "disableLoggingDeprecationUrlParameterValue";
    private static final String ENCODING = "UTF-8";

    private volatile boolean allowUrlParameterValue = false;
    private volatile boolean disableLoggingDeprecationUrlParameterValue = false;

    @Override
    public void init(final FilterConfig config)
    {
        super.init(config);
        final String configValue = config.getInitParameter(ALLOW_URL_PARAMETER_VALUE_PARAMETER_NAME);
        if (isNotBlank(configValue))
        {
            setAllowUrlParameterValue(Boolean.parseBoolean(configValue));
        }
        final String deprecationValue = config.getInitParameter(
                DISABLE_LOGGING_DEPRECATION_URL_PARAMETER_VALUE_PARAMETER_NAME);
        if (isNotBlank(deprecationValue))
        {
            setDisableLoggingDeprecationUrlParameterValue(Boolean.parseBoolean(deprecationValue));
        }
    }

    /**
     * Sets the value of allowUrlParameterValue.
     * @param allowUrlParameterValue if true then url parameter values for username
     *                               and password will be accepted.
     *                               If false url parameter values for username and
     *                               password will not be accepted.
     *
     * @since 3.1.0
     */
    public void setAllowUrlParameterValue(boolean allowUrlParameterValue)
    {
        this.allowUrlParameterValue = allowUrlParameterValue;
    }


    /**
     * Sets the value of disableLoggingDeprecationUrlParameterValue.
     * @param disableLoggingDeprecationUrlParameterValue if true then deprecated url parameter
     *                                               usage will not be logged.
     *                                               If false then deprecated url parameter
     *                                               usage will be logged.
     *
     * @since 4.0.2
     */
    public void setDisableLoggingDeprecationUrlParameterValue(boolean disableLoggingDeprecationUrlParameterValue)
    {
        this.disableLoggingDeprecationUrlParameterValue = disableLoggingDeprecationUrlParameterValue;
    }


    @Override
    protected UserPasswordPair extractUserPasswordPair(HttpServletRequest request)
    {
        // check for parameters
        String username = request.getParameter(RequestParameterConstants.OS_USERNAME);
        String password = request.getParameter(RequestParameterConstants.OS_PASSWORD);
        boolean persistentLogin = "true".equals(request.getParameter(RequestParameterConstants.OS_COOKIE));
        if (isNotEmpty(password) && hasOsPasswordQueryParam(request))
        {
            if (!allowUrlParameterValue)
            {
                log.info("Not accepting an authentication attempt for user \"{}\", as authentication" +
                        " url parameter values are not being accepted.", username);
                return null;
            }
            if (!disableLoggingDeprecationUrlParameterValue)
            {
                log.info("User \"{}\" authenticated using {} as a query parameter, " +
                                "this means of authentication has been deprecated.",
                        username, RequestParameterConstants.OS_PASSWORD);
            }
        }
        return new UserPasswordPair(username, password, persistentLogin);
    }

    private static boolean hasOsPasswordQueryParam(final HttpServletRequest request)
    {
        if (request.getQueryString() == null)
        {
            return false;
        }
        final String decodedQueryString = decodeQueryString(request.getQueryString());
        return decodedQueryString.contains(RequestParameterConstants.OS_PASSWORD + '=');
    }

    private static String decodeQueryString(final String queryString)
    {
        try
        {
            return URLDecoder.decode(queryString, ENCODING);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new AssertionError(e);
        }
    }
}
