package com.atlassian.seraph.filter;

import com.atlassian.seraph.RequestParameterConstants;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.AuthenticationContextAwareAuthenticator;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.SessionInvalidator;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.seraph.util.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This is a base authentication filter. It delegates the actual login process to a child class but takes care of the
 * redirection process.
 * <p>
 * If the authentication is successful, the user will be redirected by the filter to the URL given by the session
 * attribute at SecurityFilter.ORIGINAL_URL_KEY.
 * <p>
 * If this URL doesn't exist, it will look for a parameter 'os_destination' to use as the redirected URL instead.
 * <p>
 * If neither is found, it is assumed that the page will check the authorisation status and handle redirection itself.
 * <p>
 * From the any other filter in the request, or the servlet/JSP/action which processes the request, you can look up the
 * status of the authorisation attempt. The status is a String request attribute, with the key 'os_authstatus'.
 * <p>
 * The possible statuses are:
 * <ul>
 * <li> LoginFilter.LOGIN_SUCCESS - the login was processed, and user was logged in </li>
 * <li>LoginFilter.LOGIN_FAILURE - the login was processed, the user gave a bad username or password </li>
 * <li>LoginFilter.LOGIN_ERROR - the login was processed, an exception occurred trying to log the user in </li>
 * <li>LoginFilter.LOGIN_NOATTEMPT - the login was no processed, no form parameters existed </li>
 * </ul>
 */
public abstract class BaseLoginFilter implements Filter
{
    private static final Logger log = LoggerFactory.getLogger(BaseLoginFilter.class);

    private FilterConfig filterConfig = null;

    public static final String LOGIN_SUCCESS = "success";
    public static final String LOGIN_FAILED = "failed";
    public static final String LOGIN_ERROR = "error";
    public static final String LOGIN_NOATTEMPT = null;
    public static final String OS_AUTHSTATUS_KEY = "os_authstatus";
    public static final String AUTHENTICATION_ERROR_TYPE = "auth_error_type";
    private SecurityConfig securityConfig = null;

    public BaseLoginFilter()
    {
        super();
    }

    @Override
    public void init(final FilterConfig config)
    {
        this.filterConfig = config;
    }

    @Override
    public void destroy()
    {
        filterConfig = null;
    }

    @Override
    public void doFilter(final ServletRequest servletRequest, final ServletResponse servletResponse, final FilterChain filterChain)
            throws IOException, ServletException
    {
        final String METHOD = "doFilter : ";
        final boolean dbg = log.isDebugEnabled();

        // wrap the request with one that returns the User as the Principal
        HttpServletRequest httpServletRequest = new SecurityHttpRequestWrapper((HttpServletRequest) servletRequest);
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        if (!SecurityUtils.isSeraphFilteringDisabled(httpServletRequest) && getSecurityConfig().getController().isSecurityEnabled())
        {
            SecurityUtils.disableSeraphFiltering(httpServletRequest);

            httpServletRequest.setAttribute(OS_AUTHSTATUS_KEY, LOGIN_NOATTEMPT);

            if (dbg)
            {
                log.debug(METHOD + "____ Attempting login for : '" + getRequestUrl(httpServletRequest) + "'");
            }
            //
            // this will call the derived classes to perform the actual login process
            //
            String status = login(httpServletRequest, httpServletResponse);
            httpServletRequest.setAttribute(OS_AUTHSTATUS_KEY, status);
            if (dbg)
            {
                final String userName = httpServletRequest.getRemoteUser();
                log.debug(METHOD + "Login completed for '" + userName + "' - " + OS_AUTHSTATUS_KEY + " = '" + status + "'");
            }

            // if we successfully logged in - look for an original URL to forward to
            if (LOGIN_SUCCESS.equals(status) && redirectToOriginalDestination(httpServletRequest, httpServletResponse))
            {
                // if a redirect happened, this should return immediately
                // see https://jira.atlassian.com/browse/SER-174
                return;
            }
            // NOTE : LOGIN_NOATTEMPT is a symbolic constant for null which is a language level symbolic constant for...well...null
            //noinspection StringEquality
            if (status == LOGIN_NOATTEMPT && redirectIfUserIsAlreadyLoggedIn(httpServletRequest, httpServletResponse))
            {
                // if a redirect happened, this should return immediately
                // see https://jira.atlassian.com/browse/SER-174
                return;
            }
        }
        else if(getSecurityConfig().isInvalidateSessionOnWebsudo() &&
                httpServletRequest.getAttribute(getSecurityConfig().getWebsudoRequestKey()) != null)
        {
            if (dbg)
            {
                log.debug(METHOD + "____ Invalidating session for websudo");
            }

            SessionInvalidator si = new com.atlassian.seraph.auth.SessionInvalidator(getSecurityConfig().getInvalidateWebsudoSessionExcludeList());
            si.invalidateSession(httpServletRequest);
        }

        // NOTE: this code path should not be reached if httpServletResponse.sendRedirect has been called
        // see https://jira.atlassian.com/browse/SER-174
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getRequestUrl(final HttpServletRequest httpServletRequest)
    {
        return httpServletRequest.getServletPath() +
                (httpServletRequest.getPathInfo() == null ? "" : httpServletRequest.getPathInfo()) +
                (httpServletRequest.getQueryString() == null ? "" : "?" + httpServletRequest.getQueryString());
    }

    private boolean redirectIfUserIsAlreadyLoggedIn(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws IOException
    {
        /*
          Allow a redirect for an already logged in user under the following conditions
            1: They are already logged in
            2: They have os_destination in their request parameters
            3: They do NOT have seraph_originalurl in their session. This avoids circular redirects when a User
            tries to access a URL they do not have (role-based) privileges to view.
        */
        if (httpServletRequest.getParameterMap().get(RequestParameterConstants.OS_DESTINATION) != null)
        {
            Principal principal = getAuthenticator().getUser(httpServletRequest, httpServletResponse);
            if (principal != null)
            {
                HttpSession session = httpServletRequest.getSession();
                if (session != null && session.getAttribute(SecurityConfigFactory.getInstance().getOriginalURLKey()) == null)
                {
                    return redirectToOriginalDestination(httpServletRequest, httpServletResponse);
                }
            }
        }

        return false;
    }

    /**
     * Performs the actual authentication (if required) and returns the status code. Status code is chosen to be one of
     * these:
     * <p>
     * The possible statuses are:
     * <ul>
     * <li> BaseLoginFilter.LOGIN_SUCCESS - the login was processed, and user was logged in
     * <li> BaseLoginFilter.LOGIN_FAILURE - the login was processed, the user gave a bad username or password
     * <li> BaseLoginFilter.LOGIN_ERROR - the login was processed, an exception occurred trying to log the user in
     * <li> BaseLoginFilter.LOGIN_NOATTEMPT - the login was no processed, no form parameters existed
     * </ul>
     * <p>
     * When there is an error on login, implementations should set a request attribute with name
     * {@link #AUTHENTICATION_ERROR_TYPE} and a type of {@link com.atlassian.seraph.auth.AuthenticationErrorType} in
     * order to indicate the type of error.
     *
     * @param httpServletRequest  the HTTP request in play
     * @param httpServletResponse the HTTP response in play
     *
     * @return authentication status
     */
    public abstract String login(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);

    /**
     * This request wrapper class overrrides the {@link javax.servlet.http.HttpServletRequest#getRemoteUser()} and
     * {@link javax.servlet.http.HttpServletRequest#getUserPrincipal()} methods and calls the current {@link
     * com.atlassian.seraph.auth.Authenticator} to provide these values.
     */
    class SecurityHttpRequestWrapper extends HttpServletRequestWrapper
    {
        private HttpServletRequest delegateHttpServletRequest;

        public SecurityHttpRequestWrapper(final HttpServletRequest delegateHttpServletRequest)
        {
            super(delegateHttpServletRequest);
            this.delegateHttpServletRequest = delegateHttpServletRequest;
        }

        @Override
        public String getRemoteUser()
        {
            Principal user = getUserPrincipal();
            return (user == null) ? null : user.getName();
        }

        @Override
        public Principal getUserPrincipal()
        {
            if(getAuthenticator().getClass().isAnnotationPresent(AuthenticationContextAwareAuthenticator.class))
            {
                return getAuthenticationContext().getUser();
            }
            else
            {
                return getAuthenticator().getUser(delegateHttpServletRequest);
            }
        }
    }

    /**
     * Redirect the response to the original destination if present
     *
     * @param httpServletRequest  the HTTP request in play
     * @param httpServletResponse the HTTP response in play
     *
     * @return true if a redirect was needed and issued
     *
     * @throws IOException If the redirect throws IOException. See {@link HttpServletResponse#sendRedirect(String)}
     */
    protected boolean redirectToOriginalDestination(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws IOException
    {
        final String METHOD = "redirectToOriginalDestination : ";
        final boolean dbg = log.isDebugEnabled();

        // The request parameters take precedence, followed by the session
        String redirectURL = httpServletRequest.getParameter(RequestParameterConstants.OS_DESTINATION);
        final String originalURLKey = getSecurityConfig().getOriginalURLKey();
        final HttpSession httpSession = httpServletRequest.getSession();
        if (redirectURL == null)
        {
            redirectURL = (String) httpSession.getAttribute(originalURLKey);
        }
        // clear the session if both are set
        httpSession.removeAttribute(originalURLKey);

        if (redirectURL == null)
        {
            return false;
        }

        // Check redirect for header injection and potential phishing attack - see SER-127 and SER-128
        if (!getSecurityConfig().getRedirectPolicy().allowedRedirectDestination(redirectURL, httpServletRequest))
        {
            // Redirect Destination is not allowed.
            log.warn(METHOD + "Redirect request to '" + redirectURL + "' is not allowed. Will send user to the context root instead.");
            // note that the context path will get added below.
            redirectURL = "/";
        }

        if (!isAbsoluteUrl(redirectURL))
        {
            // Reading the javadoc for HttpServletResponse.sendRedirect() leads me to believe that this is overkill or wrong.
            // We should just leave the leading slash of relative paths, and the Servlet container will interpret the context for us.
            redirectURL = RedirectUtils.appendPathToContext(httpServletRequest.getContextPath(), redirectURL);
        }

        if (dbg)
        {
            log.debug(METHOD + "Login redirect to: " + redirectURL);
        }

        httpServletResponse.sendRedirect(redirectURL);
        return true;
    }

    protected boolean isAbsoluteUrl(final String url)
    {
        try
        {
            URI uri = new URI(url);
            return uri.getHost() != null;
        }
        catch (URISyntaxException e)
        {
            return false;
        }
    }

    protected Authenticator getAuthenticator()
    {
        return getSecurityConfig().getAuthenticator();
    }

    protected ElevatedSecurityGuard getElevatedSecurityGuard()
    {
        return getSecurityConfig().getElevatedSecurityGuard();
    }

    protected SecurityConfig getSecurityConfig()
    {
        if (securityConfig == null)
        {
            securityConfig = (SecurityConfig) filterConfig.getServletContext().getAttribute(SecurityConfig.STORAGE_KEY);
        }
        return securityConfig;
    }

    protected AuthenticationContext getAuthenticationContext()
    {
        return getSecurityConfig().getAuthenticationContext();
    }
}
