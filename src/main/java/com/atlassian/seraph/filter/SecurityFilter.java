package com.atlassian.seraph.filter;

import com.atlassian.seraph.SecurityService;
import com.atlassian.seraph.auth.AuthType;
import com.atlassian.seraph.auth.AuthenticationContext;
import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.seraph.util.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * The SecurityFilter determines which roles are required for a given request by querying all of the Services.
 *
 * @see SecurityService
 */
public class SecurityFilter implements Filter
{
    private FilterConfig config = null;
    private SecurityConfig securityConfig = null;

    private static final Logger log = LoggerFactory.getLogger(SecurityFilter.class);
    static final String ALREADY_FILTERED = "os_securityfilter_already_filtered";
    public static final String ORIGINAL_URL = "atlassian.core.seraph.original.url";

    @Override
    public void init(final FilterConfig config)
    {
        log.debug("SecurityFilter.init");
        this.config = config;

        String configFileLocation = null;

        if (config.getInitParameter("config.file") != null)
        {
            configFileLocation = config.getInitParameter("config.file");
            log.debug("Security config file location: " + configFileLocation);
        }

        securityConfig = SecurityConfigFactory.getInstance(configFileLocation);
        config.getServletContext().setAttribute(SecurityConfig.STORAGE_KEY, securityConfig);
        log.debug("SecurityFilter.init completed successfully.");
    }

    @Override
    public void destroy()
    {
        log.debug("SecurityFilter.destroy");
        // SER-129: securityConfig was seen in the wild to sometimes throw NPE.
        if (securityConfig == null)
        {
            log.warn("Trying to destroy a SecurityFilter with null securityConfig.");
        }
        else
        {
            // TODO: Why do we set these variables to null? Note that the securityConfig still lives in the ServletContext attribute.
            securityConfig.destroy();
            securityConfig = null;
        }
        config = null;
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain) throws IOException, ServletException
    {
        if ((req.getAttribute(ALREADY_FILTERED) != null) || !getSecurityConfig().getController().isSecurityEnabled())
        {
            chain.doFilter(req, res);
            return;
        }
        else
        {
            req.setAttribute(ALREADY_FILTERED, Boolean.TRUE);
        }

        final String METHOD = "doFilter : ";
        final boolean dbg = log.isDebugEnabled();

        // Try and get around Orion's bug when redeploying
        // it seems that filters are not called in the correct order
        if (!SecurityUtils.isSeraphFilteringDisabled(req))
        {
            log.warn(METHOD + "LoginFilter not yet applied to this request - terminating filter chain");
            return;
        }

        final HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        final HttpServletResponse httpServletResponse = (HttpServletResponse) res;

        final String originalURL = httpServletRequest.getServletPath() + (httpServletRequest.getPathInfo() == null ? "" : httpServletRequest.getPathInfo()) + (httpServletRequest.getQueryString() == null ? "" : "?" + httpServletRequest.getQueryString());

        // store the original URL as a request attribute anyway - often useful for pages to access it (ie login links)
        httpServletRequest.setAttribute(SecurityFilter.ORIGINAL_URL, originalURL);
        if (dbg)
        {
            log.debug(METHOD + "Storing the originally requested URL (" + SecurityFilter.ORIGINAL_URL + "=" + originalURL + ")");
        }

        final Set<String> requiredRoles = new HashSet<String>();
        final Set<String> missingRoles = new HashSet<String>();

        // loop through loaded services and get required roles
        for (final SecurityService service : getSecurityConfig().getServices())
        {
            final Set<String> serviceRoles = service.getRequiredRoles(httpServletRequest);
            requiredRoles.addAll(serviceRoles);
        }

        if (dbg)
        {
            log.debug(METHOD + "requiredRoles = " + requiredRoles);
        }

        // whether this URL needs authorisation
        boolean needAuth = false;

        // try to get the user (for cookie logins and basic auth as well as already established sessions)
        final Authenticator authenticator = getSecurityConfig().getAuthenticator();
        final Principal user = authenticator.getUser(httpServletRequest, httpServletResponse);

        if (user == null)
        {
            AuthType authType = AuthType.getAuthTypeInformation(httpServletRequest, getSecurityConfig());

            // This include AuthType.BASIC *as well as* when the client pre-emptively sends BASIC credentials
            // This will also catch AuthType.ANY when they send BASIC credentials.
            if (RedirectUtils.isBasicAuthentication(httpServletRequest, getSecurityConfig().getAuthType()))
            {
                // The DefaultAuthenticator already did a response.sendError(401)
                return;
            }
            else if (authType == AuthType.COOKIE && httpServletRequest.getSession(false) == null)
            {
                httpServletResponse.sendError(401, "os_authType was 'cookie' but no valid cookie was sent.");
                return;
            }
            else if (authType == AuthType.ANY)
            {
                // If they specified AuthType.ANY and sent BASIC credentials it was handled by a previous case.
                // For this we only have to worry about cookies.
                if (hasJSessionCookie(httpServletRequest.getCookies()) && httpServletRequest.getSession(false) == null)
                {
                    httpServletResponse.sendError(401, "os_authType was 'any' and an invalid cookie was sent.");
                    return;
                }
            }
        }

        // set the user in the context
        if (dbg)
        {
            log.debug(METHOD + "Setting Auth Context to be '" + (user == null ? "anonymous " : user.getName()) + "'");
        }

        final AuthenticationContext authenticationContext = getAuthenticationContext();
        authenticationContext.setUser(user);

        // check if the current user has all required permissions
        // if there is no current user, request.isUserInRole() always returns false so this works
        for (final Object element : requiredRoles)
        {
            final String role = (String) element;

            // this isUserInRole method is only used here and 'd be better off replaced by getRoleMapper().hasRole(user, request, role)) since we have the user already
            // was : if (!securityConfig.getAuthenticator().isUserInRole(request, role))
            if (!getSecurityConfig().getRoleMapper().hasRole(user, httpServletRequest, role))
            {
                log.info(METHOD + "'" + user + "' needs (and lacks) role '" + role + "' to access " + originalURL);
                needAuth = true;
                missingRoles.add(role);
            }
        }

        // check if we're at the signon page, in which case do not auth
        if ((httpServletRequest.getServletPath() != null) && httpServletRequest.getServletPath().equals(getSecurityConfig().getLoginURL()))
        {
            if (dbg)
            {
                log.debug(METHOD + "Login page requested so no additional authorization required.");
            }
            needAuth = false;
        }

        // if we need to authenticate, store current URL and forward
        if (needAuth)
        {
            // POST requests can't be properly handled by 30x redirects, since all the POST parameters are lost in
            // the redirect process. to get around this Seraph supports a "login forward path". if this is defined
            // Seraph uses the servlet request dispatcher to FORWARD the request, giving apps a chance to save  or
            // otherwise handle the POST params before redirecting to the login page.
            String loginForwardPath = getSecurityConfig().getLoginForwardPath();
            if (isPOST(httpServletRequest) && isNotBlank(loginForwardPath))
            {
                if (dbg)
                {
                    log.debug(METHOD + "Need Authentication for POST: Forwarding to: " + loginForwardPath + " from: " + originalURL);
                }

                httpServletRequest.getRequestDispatcher(loginForwardPath).forward(httpServletRequest, httpServletResponse);
                return;
            }

            if (dbg)
            {
                log.debug(METHOD + "Need Authentication: Redirecting to: " + getSecurityConfig().getLoginURL() + " from: " + originalURL);
            }

            httpServletRequest.getSession().setAttribute(getSecurityConfig().getOriginalURLKey(), originalURL);
            // only redirect if we can. if isCommited==true, there might have been a redirection requested by a LoginInterceptor, for instance.
            if (!httpServletResponse.isCommitted())
            {
                httpServletResponse.sendRedirect(getLoginUrl(httpServletRequest, missingRoles));
            }
            // Suppress the IDEA compile warning. This return is unnecessary, but good for safety against future code changes.
            //noinspection UnnecessaryReturnStatement
            return;
        }
        else
        {
            try
            {
                chain.doFilter(req, res);
            }
            finally
            {
                // clear the user from the context
                authenticationContext.clearUser();
            }
        }
    }

    protected String getLoginUrl(final HttpServletRequest httpServletRequest, Set<String> missingRoles)
    {
        return RedirectUtils.getLoginUrl(httpServletRequest);
    }

    private boolean isPOST(HttpServletRequest httpServletRequest)
    {
        return "POST".equals(httpServletRequest.getMethod());
    }

    private boolean hasJSessionCookie(final Cookie[] cookies)
    {
        if (cookies == null)
        {
            return false;
        }

        for (Cookie cookie : cookies)
        {
            if (cookie.getName().equals("JSESSIONID"))
            {
                return true;
            }
        }

        return false;
    }

    protected SecurityConfig getSecurityConfig()
    {
        // ?? is this any useful, since we already initalize this in the init method ??
        if (securityConfig == null)
        {
            securityConfig = (SecurityConfig) config.getServletContext().getAttribute(SecurityConfig.STORAGE_KEY);
        }
        return securityConfig;
    }

    protected AuthenticationContext getAuthenticationContext()
    {
        return getSecurityConfig().getAuthenticationContext();
    }

}
