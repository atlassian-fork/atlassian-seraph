package com.atlassian.seraph.ioc;

import com.atlassian.seraph.service.rememberme.NoopRememberMeService;
import com.atlassian.seraph.service.rememberme.RememberMeService;

/**
 * This class represents an extension point for Seraph. Applications that use Seraph library can register implementations of service interfaces
 * defined by Seraph. To register an implementation host application must call an appropriate setter method eg. {@link #setRememberMeService(RememberMeService)}
 * Once host application registers a service Seraph will use the registered service. Host application may chose not to register a service,
 * in such case Seraph will use default Noop implementation of it. E.g. if a host application does not set a {@link RememberMeService} through
 * {@link #setRememberMeService(RememberMeService)} Seraph will use a {@link NoopRememberMeService}  implementation of {@link RememberMeService}.
 *
 * If in the future Seraph defines new SPIs and expects host application to provide implementations for them then this class will have to add
 * setters and getters definitions for new interfaces.
 *
 * @author anatoli
 *
 */
public class ApplicationServicesRegistry
{
    private static volatile RememberMeService rememberMeService = NoopRememberMeService.INSTANCE;

    /**
     * This method provides the ability for a host application to set an implementation of {@link RememberMeService} for Seraph to use.
     * If host application does not set its implementation of {@link RememberMeService} Seraph will use a {@link NoopRememberMeService}.
     *
     * @param rememberMeService the {@link RememberMeService} implemenation for Seraph to use.
     *
     * @throws IllegalArgumentException if argument is null
     */
    public static void setRememberMeService(RememberMeService rememberMeService)
    {
        if (rememberMeService == null)
        {
            throw new IllegalArgumentException("rememberMeService must not be null.");
        }
        ApplicationServicesRegistry.rememberMeService = rememberMeService;
    }

    /**
     * Returns a RememberMeService that was set by an application via {@link #setRememberMeService(RememberMeService)}
     * or a {@link NoopRememberMeService} if application has not set one. Will never return null.
     * @return a {@link RememberMeService} or {@link NoopRememberMeService} if an application has not set one.
     */
    public static RememberMeService getRememberMeService()
    {
        return rememberMeService;
    }
}
