package com.atlassian.seraph.elevatedsecurity;

import com.atlassian.seraph.config.SecurityConfig;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * This {@link com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard} does nothing, as its name might suggest
 *
 * @since v2.1
 */
public class NoopElevatedSecurityGuard implements ElevatedSecurityGuard
{
    /**
     * A singleton instance of NoopElevatedSecurityGuard that does nothing!
     */
    public static final NoopElevatedSecurityGuard INSTANCE = new NoopElevatedSecurityGuard();

    private NoopElevatedSecurityGuard()
    {
    }

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
    }

    @Override
    public boolean performElevatedSecurityCheck(final HttpServletRequest httpServletRequest, final String userName)
    {
        // by returning true we are in effect saying "they passed any checks required"!
        return true;
    }

    @Override
    public void onFailedLoginAttempt(final HttpServletRequest httpServletRequest, final String userName)
    {
    }

    @Override
    public void onSuccessfulLoginAttempt(final HttpServletRequest httpServletRequest, final String userName)
    {
    }
}
