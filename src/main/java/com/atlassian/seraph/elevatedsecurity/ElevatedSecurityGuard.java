package com.atlassian.seraph.elevatedsecurity;

import com.atlassian.seraph.Initable;

import javax.servlet.http.HttpServletRequest;

/**
 * An ElevatedSecurityGaurd is responsible for checking whether a user has failed authentication too many times and
 * hence needs to pass an "elevated" security check before they can authenticate again.
 *
 * @since v2.1
 */
public interface ElevatedSecurityGuard extends Initable
{

    /**
     * This will be called to perform an elevated security check for a given user name.  Its up to the implementor to
     * decide what if any tests needs to be done.  It should return true if the authentication can proceed.
     *
     * @param httpServletRequest the HTTP request in play
     * @param userName           the name of the user to get login information about
     *
     * @return true if the user passed the elevated security check or false if not.  If you dont want any elevated security
     * checks done them always return true.
     */
    boolean performElevatedSecurityCheck(HttpServletRequest httpServletRequest, String userName);

    /**
     * This is called when a user fails a login check, either because they failed the elevated security check or they
     * failed the more basic username and password check.
     * <p>
     * The username MAY be null if a valid username cannot be found for example
     *
     * @param httpServletRequest the HTTP request in play
     * @param userName           the name of the user to get login information about
     */
    void onFailedLoginAttempt(HttpServletRequest httpServletRequest, String userName);

    /**
     * This is called when a user passes a login check.
     * <p>
     * The username MAY be null if a valid username cannot be found for example
     *
     * @param httpServletRequest the HTTP request in play
     * @param userName           the name of the user to get login information about
     */
    void onSuccessfulLoginAttempt(HttpServletRequest httpServletRequest, String userName);
}
