package com.atlassian.seraph;

import java.io.Serializable;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

/**
 * A SecurityService determines which roles a user is required to have to access a resource.
 *
 * Two services are provided with Seraph by default, {@link com.atlassian.seraph.service.PathService}
 * and {@link com.atlassian.seraph.service.WebworkService}.
 */
public interface SecurityService extends Serializable, Initable
{
    void destroy();

    Set<String> getRequiredRoles(HttpServletRequest request);
}
