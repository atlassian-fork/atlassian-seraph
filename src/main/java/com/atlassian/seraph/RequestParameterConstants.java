package com.atlassian.seraph;

/**
 * This is used to reference constant strings used as request parameters names.
 */
public class RequestParameterConstants
{
    public static final String OS_DESTINATION = "os_destination";
    public static final String OS_USERNAME = "os_username";
    public static final String OS_PASSWORD = "os_password";
    public static final String OS_COOKIE = "os_cookie";
}
