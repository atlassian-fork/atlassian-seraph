package com.atlassian.seraph.service.rememberme;

import com.atlassian.security.random.DefaultSecureTokenGenerator;

/**
 * A default implementation of {@link RememberMeTokenGenerator}  that should be enough for most apps
 * <p>
 */
public class DefaultRememberMeTokenGenerator implements RememberMeTokenGenerator
{

    @Override
    public RememberMeToken generateToken(final String userName)
    {
        final String base64 = DefaultSecureTokenGenerator.getInstance().generateToken();
        return DefaultRememberMeToken.builder(base64).setUserName(userName).setCreatedTime(System.currentTimeMillis()).build();
    }
}
