package com.atlassian.seraph.service.rememberme;

import com.atlassian.security.utils.ConstantTimeComparison;
import com.atlassian.seraph.ioc.ApplicationServicesRegistry;
import com.atlassian.seraph.spi.rememberme.RememberMeConfiguration;
import com.atlassian.seraph.spi.rememberme.RememberMeTokenDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * This default RememberMeService needs to have a certain SPI implementations into it so that it can function.  This is
 * what the application needs to provide.  Most of the other default implementations can be used as is.
 */
public class DefaultRememberMeService implements RememberMeService
{
    private static final Logger log = LoggerFactory.getLogger(DefaultRememberMeService.class);

    private final RememberMeConfiguration rememberMeConfiguration;
    private final RememberMeTokenDao rememberMeTokenDao;
    private final RememberMeTokenGenerator rememberMeTokenGenerator;

    public DefaultRememberMeService(final RememberMeConfiguration rememberMeConfiguration, final RememberMeTokenDao rememberMeTokenDao, final RememberMeTokenGenerator rememberMeTokenGenerator)
    {
        this.rememberMeConfiguration = rememberMeConfiguration;
        this.rememberMeTokenDao = rememberMeTokenDao;
        this.rememberMeTokenGenerator = rememberMeTokenGenerator;
        ApplicationServicesRegistry.setRememberMeService(this);
    }

    @Override
    public String getRememberMeCookieAuthenticatedUsername(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
    {
        // do they have the remember me cookie set
        RememberMeToken cookieToken = getCookieValue(httpServletRequest);
        if (cookieToken != null)
        {
            // yes they do so lets check it against the app
            RememberMeToken storedToken = rememberMeTokenDao.findById(cookieToken.getId());
            if (storedToken != null)
            {
                if (ConstantTimeComparison.isEqual(cookieToken.getRandomString(), storedToken.getRandomString()) && !isExpired(storedToken))
                {
                    return storedToken.getUserName();
                }
            }

            if (httpServletResponse != null)
            {
                // ok they token is not valid so we need to remove it from the request
                removeRememberMeCookie(httpServletRequest, httpServletResponse);
            }
        }
        return null;
    }

    private boolean isExpired(RememberMeToken storedToken)
    {
        return storedToken.getCreatedTime() + TimeUnit.SECONDS.toMillis(rememberMeConfiguration.getCookieMaxAgeInSeconds()) < System.currentTimeMillis();
    }

    @Override
    public void addRememberMeCookie(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final String authenticatedUsername)
    {
        final RememberMeToken token = rememberMeTokenGenerator.generateToken(authenticatedUsername);
        final RememberMeToken persistedToken = rememberMeTokenDao.save(token);

        final String desiredCookieName = rememberMeConfiguration.getCookieName();
        Cookie cookie = findRememberCookie(httpServletRequest, desiredCookieName);
        if (cookie == null)
        {
            cookie = new Cookie(desiredCookieName, persistedToken.getRandomString());
        }
        setValuesIntoCookie(httpServletRequest, cookie, toCookieValue(persistedToken),
                rememberMeConfiguration.getCookieMaxAgeInSeconds(),
                rememberMeConfiguration.getCookieDomain(httpServletRequest),
                rememberMeConfiguration.getCookiePath(httpServletRequest),
                rememberMeConfiguration.isInsecureCookieAlwaysUsed()
        );
        setRememberMeCookie(httpServletRequest, httpServletResponse, cookie);
    }

    @Override
    public void removeRememberMeCookie(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
    {
        final Cookie cookie = findRememberCookie(httpServletRequest, rememberMeConfiguration.getCookieName());
        if (cookie != null)
        {
            RememberMeToken cookieToken = parseIntoToken(cookie);

            setValuesIntoCookie(httpServletRequest, cookie, "", 0,
                    rememberMeConfiguration.getCookieDomain(httpServletRequest),
                    rememberMeConfiguration.getCookiePath(httpServletRequest),
                    rememberMeConfiguration.isInsecureCookieAlwaysUsed()
            );
            setRememberMeCookie(httpServletRequest, httpServletResponse, cookie);

            // and now remove it from the application store as well
            if (cookieToken != null)
            {
                rememberMeTokenDao.remove(cookieToken.getId());
            }
        }
    }

    private void setValuesIntoCookie(final HttpServletRequest httpServletRequest, final Cookie cookie,
        final String value, final int maxAgeInSeconds, final String cookieDomain,
        final String cookiePath, final boolean isInsecureCookieUsed)
    {
        if (isNotBlank(cookieDomain))
        {
            cookie.setDomain(cookieDomain);
        }
        if (isNotBlank(cookiePath))
        {
            cookie.setPath(cookiePath);
        }
        if (!isInsecureCookieUsed)
        {
            cookie.setSecure(httpServletRequest.isSecure());
        }
        cookie.setMaxAge(maxAgeInSeconds);
        cookie.setValue(escapeInvalidCookieCharacters(value));
    }

    private void setRememberMeCookie(final HttpServletRequest httpServletRequest,
        final HttpServletResponse httpServletResponse, final Cookie cookie)
    {
        if (rememberMeConfiguration.isCookieHttpOnly(httpServletRequest))
        {
            cookie.setHttpOnly(true);
        }
        httpServletResponse.addCookie(cookie);
    }

    private String toCookieValue(final RememberMeToken persistedToken)
    {
        return persistedToken.getId() + ":" + persistedToken.getRandomString();
    }

    /**
     * Returns the value of the remember me cookie if its present ot NULL if its not there
     *
     * @param httpServletRequest the request in play
     *
     * @return the RememberMeToken  or null if the cookie is not there or is not a valid value
     */
    private RememberMeToken getCookieValue(final HttpServletRequest httpServletRequest)
    {
        final Cookie cookie = findRememberCookie(httpServletRequest, rememberMeConfiguration.getCookieName());
        if (cookie != null)
        {
            return parseIntoToken(cookie);
        }
        return null;
    }

    private RememberMeToken parseIntoToken(final Cookie cookie)
    {
        final String value = unescapeInvalidCookieCharacters(cookie.getValue());
        if (isBlank(value))
        {
            return null;
        }
        int indexColon = value.indexOf(':');
        if (indexColon <= 0 || indexColon == value.length() - 1)
        {
            return null;
        }
        Long id;
        try
        {
            id = Long.parseLong(value.substring(0, indexColon));
        }
        catch (NumberFormatException e)
        {
            return null;
        }
        String randomString = value.substring(indexColon + 1);
        return DefaultRememberMeToken.builder(id, randomString).build();
    }

    private Cookie findRememberCookie(HttpServletRequest httpServletRequest, final String cookieName)
    {
        final Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null)
        {
            for (Cookie cookie : cookies)
            {
                if (cookieName.equalsIgnoreCase(cookie.getName()))
                {
                    return cookie;
                }
            }
        }
        return null;

    }

    private static final String URL_ENCODING = "UTF-8";

    /**
     * Escape invalid cookie characters, see SER-117
     *
     * @param s the String to escape characters for.
     *
     * @return the encoded string.
     *
     * @see #unescapeInvalidCookieCharacters(String)
     */
    private static String escapeInvalidCookieCharacters(final String s)
    {
        try
        {
            return URLEncoder.encode(s, URL_ENCODING);
        }
        catch (final UnsupportedEncodingException e)
        {
            throw new AssertionError(e);
        }
    }

    /**
     * Un-escape invalid cookie characters, see SER-117
     *
     * @param s the String to escape characters for.
     *
     * @return the encoded string.
     *
     * @see #escapeInvalidCookieCharacters(String)
     */
    private static String unescapeInvalidCookieCharacters(final String s)
    {
        try
        {
            return URLDecoder.decode(s, URL_ENCODING);
        }
        catch (final UnsupportedEncodingException e)
        {
            log.error("UTF-8 encoding unsupported !!?!! How is that possible in java?", e);
            throw new AssertionError(e);
        }
    }
}
