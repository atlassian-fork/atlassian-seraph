package com.atlassian.seraph.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Implement this interface to intercept logout methods.
 */
public interface LogoutInterceptor extends Interceptor
{
    void beforeLogout(HttpServletRequest request, HttpServletResponse response);

    void afterLogout(HttpServletRequest request, HttpServletResponse response);
}
