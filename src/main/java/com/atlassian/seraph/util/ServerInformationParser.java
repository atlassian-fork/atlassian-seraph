package com.atlassian.seraph.util;

/**
 * Parses {@code ServletContext.getServerInfo()} into separate pieces.
 * 
 * @since 2.4.0
 */
public class ServerInformationParser
{
    /**
     * Given a String in the expected format, returns parsed server information.
     * The textual server information must be in the format of "ServerName/ServerVersion (OtherInformation)".
     * For example, Apache Tomcat 6.0.20 would be formatted as "Apache Tomcat/6.0.20".
     * The "other information" part is optional.
     *
     * http://download.oracle.com/javaee/6/api/javax/servlet/ServletContext.html#getServerInfo%28%29
     *
     * @param serverInformationText the textual version of the server information
     * @return parsed server information.
     */
    public static ServerInformation parse(String serverInformationText)
    {
        if (!serverInformationText.contains("/"))
        {
            throw new IllegalArgumentException("Server information is not present: " + serverInformationText);
        }

        int slashLoc = serverInformationText.indexOf("/");
        String name = serverInformationText.substring(0, slashLoc);

        //test for other information
        if (serverInformationText.indexOf(" ", slashLoc) == -1)
        {
            //no other information exists
            String version = serverInformationText.substring(slashLoc + 1);
            return new ServerInformation(name, version);
        }
        else
        {
            //other information exists
            int spaceLoc = serverInformationText.indexOf(" ", slashLoc);
            String version = serverInformationText.substring(slashLoc + 1, spaceLoc);
            String otherInformation = serverInformationText.substring(spaceLoc + 2, serverInformationText.length() - 1); //remove the parens
            return new ServerInformation(name, version, otherInformation);
        }
    }

    /**
     * Holder for easily accessible information about a server.
     *
     * Every server has at least a name and a version. {@code otherInformation} is null if not specified.
     */
    public static class ServerInformation
    {
        private final String name;
        private final String version;
        private final String otherInformation;

        private ServerInformation(String name, String version)
        {
            this(name, version, null);
        }

        private ServerInformation(String name, String version, String otherInformation)
        {
            this.name = name;
            this.version = version;
            this.otherInformation = otherInformation;
        }

        public String getName()
        {
            return name;
        }

        public String getVersion()
        {
            return version;
        }

        public String getOtherInformation()
        {
            return otherInformation;
        }

        public boolean isApacheTomcat()
        {
            return "Apache Tomcat".equals(name);
        }
    }
}
