package com.atlassian.seraph.spi.rememberme;

import com.atlassian.seraph.service.rememberme.RememberMeToken;

import java.util.List;

/**
 * Each Seraph ready application needs to provide a spi Dao to hold and return remember me cookie information against a
 * user.
 */
public interface RememberMeTokenDao
{
    /**
     * Finds the token with the given id. A caller must validate the token as it might be expired.
     * <p>
     *
     * @param tokenId the id of the token
     *
     * @return a RememberMeToken or null if one cant be found.
     */
    RememberMeToken findById(Long tokenId);

    /**
     * This will be called to save the presented token to the database against the specified user.  The id field will
     * need to be filled out during this process.
     *
     * @param token the token in play
     *
     * @return a persisted token that has the correct database id filled out
     */
    RememberMeToken save(RememberMeToken token);

    /**
     * This will return a list of the RememberMeToken that a user currently has.
     * A caller must validate the tokens as they might be expired.
     * <p>
     * This method is not used by the Seraph code per se but its specified as part of the Atlassian RememberMe tech
     * spec.
     *
     * @param userName the username to look up tokens for
     * @return a List or {@link com.atlassian.seraph.service.rememberme.RememberMeToken}s or an EMPTY list if there are none
     */
    List<RememberMeToken> findForUserName(String userName);

    /**
     * Removes the specific token given the id
     * <p>
     *
     * @param tokenId the id of the token to remove
     */
    void remove(Long tokenId);

    /**
     * Called to remove ALL tokens that are stored for the specificed user
     * <p>
     * This method is not used by the Seraph code per se but its specified as part of the Atlassian RememberMe tech
     * spec.
     *
     * @param username the username in question
     */
    void removeAllForUser(String username);

    /**
     * Can be called to remove all  tokens for all users.
     * <p>
     * This method is not used by the Seraph code per se but its specified as part of the Atlassian RememberMe tech
     * spec.
     */
    void removeAll();
}
