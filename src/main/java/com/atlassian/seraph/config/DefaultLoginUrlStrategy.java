package com.atlassian.seraph.config;

import java.util.Map;

/**
 * Login URL strategy that just returns the originally configured values.
 */
public class DefaultLoginUrlStrategy implements LoginUrlStrategy
{
    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {}

    @Override
    public String getLoginURL(final SecurityConfig config, final String configuredLoginUrl)
    {
        return configuredLoginUrl;
    }

    @Override
    public String getLogoutURL(final SecurityConfig config, final String configuredLogoutUrl)
    {
        return configuredLogoutUrl;
    }

    @Override
    public String getLinkLoginURL(final SecurityConfig config, final String configuredLinkLoginUrl)
    {
        return configuredLinkLoginUrl;
    }
}
