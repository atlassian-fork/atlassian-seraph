package com.atlassian.seraph.config;

import com.atlassian.seraph.Initable;

import javax.servlet.http.HttpServletRequest;

/**
 * This interface is included to allow fine-grained control over what URLs Seraph will allow you to redirect to.
 * <p>
 * Upon successful login, Seraph will redirect the user to a URL configured in the HTTP Session or as a request parameter.
 * In order to hinder potential phishing attacks, by default Seraph will only allow you to redirect to a URL in the same
 * context as the incoming request.
 * Applications can change this behaviour by configuring the default RedirectPolicy, or providing a custom one.
 * <p>
 * Note that applications can also take advantage of Seraph redirect checking for internal redirects.
 * To do so, they would get hold of the RedirectPolicy by calling {@link com.atlassian.seraph.config.SecurityConfig#getRedirectPolicy}  
 *
 * @since v0.38.3
 */
public interface RedirectPolicy extends Initable
{
    /**
     * Returns <code>true</code> if we are allowed to redirect to the given URL from the given HTTP request.
     * This is intended to stop malicious users from constructing URL's that would log you in to JIRA, then redirect you some where else.
     * See http://jira.atlassian.com/browse/SER-128
     *
     * @param redirectUrl The URL we are proposing to redirect to.
     * @param request The incoming HttpServletRequest.
     * @return <code>true</code> if we are allowed to redirect to the given URL from the given HTTP request.
     */
    boolean allowedRedirectDestination(String redirectUrl, HttpServletRequest request);
}
