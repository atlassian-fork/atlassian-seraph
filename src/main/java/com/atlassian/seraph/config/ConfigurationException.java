package com.atlassian.seraph.config;

/**
 * Exception thrown during Seraph configuration.
 */
public class ConfigurationException extends Exception
{
    public ConfigurationException(final String s)
    {
        super(s);
    }

    public ConfigurationException(final String s, final Throwable t)
    {
        super(s + " : " + t, t);
    }
}
