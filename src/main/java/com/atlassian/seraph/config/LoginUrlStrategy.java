package com.atlassian.seraph.config;

import com.atlassian.seraph.Initable;

/**
 * Strategy for programatically overriding the SecurityConfig's default login/logout URL configuration.
 * This is useful for applications that may only know their login URL at runtime (for example, JIRA
 * Studio)
 *
 * <p>Technically this should be a LoginLogoutUrlStrategy, but that's a really dumb name. :)
 */
public interface LoginUrlStrategy extends Initable
{
    String getLoginURL(SecurityConfig config, String configuredLoginUrl);

    String getLogoutURL(SecurityConfig config, String configuredLogoutUrl);

    String getLinkLoginURL(SecurityConfig config, String configuredLinkLoginUrl);
}
