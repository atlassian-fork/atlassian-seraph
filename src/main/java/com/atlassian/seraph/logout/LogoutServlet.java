package com.atlassian.seraph.logout;

import com.atlassian.seraph.auth.Authenticator;
import com.atlassian.seraph.auth.AuthenticatorException;
import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.config.SecurityConfigImpl;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Seraph logout servlet. Configured via the 'logout.url' init param in seraph-config.xml. This servlet supports two logout behaviours:
 * <ul>
 * <li>If 'logout.url' is a relative path (e.g. <code>/logout.jsp</code> or <code>/logout.action</code>, this servlet
 * simply redirects to it. The redirected-to page is responsible for calling {@link Authenticator#logout(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.</li>
 * <li>If 'logout.url' is absolute, this servlet logs the user out with {@link Authenticator#logout(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
 * and then redirects to the absolute URL.</li>
 * </ul>
 */
public class LogoutServlet extends HttpServlet
{
    private SecurityConfig securityConfig;

    @Override
    public void init() throws ServletException
    {
        super.init();
        securityConfig = SecurityConfigFactory.getInstance();
    }

    @Override
    public void init(final ServletConfig servletConfig) throws ServletException
    {
        super.init(servletConfig);
        securityConfig = (SecurityConfig) servletConfig.getServletContext().getAttribute(SecurityConfigImpl.STORAGE_KEY);
    }

    @Override
    protected void service(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        if (isRelativeRedirect())
        {
            // Internal logout page; we rely on it to execute the logout logic in its own good time (eg., if the user confirms logout)
            response.sendRedirect(request.getContextPath() + getSecurityConfig().getLogoutURL());
        }
        else
        {
            // External logout page; we execute logout logic immediately, and redirect to the external page.
            try
            {
                final Authenticator authenticator = getAuthenticator();
                authenticator.logout(request, response);
            }
            catch (final AuthenticatorException e)
            {
                throw new ServletException("Seraph authenticator couldn't log out", e);
            }
            response.sendRedirect(getSecurityConfig().getLogoutURL());
        }
    }

    private boolean isRelativeRedirect()
    {
        return getSecurityConfig().getLogoutURL().indexOf("://") == -1;
    }

    protected SecurityConfig getSecurityConfig()
    {
        return securityConfig;
    }

    protected Authenticator getAuthenticator()
    {
        return getSecurityConfig().getAuthenticator();
    }
}
