package com.atlassian.seraph.auth;

import java.security.Principal;

public class AuthenticationContextImpl implements AuthenticationContext
{
    private static final ThreadLocal<Principal> threadLocal = new ThreadLocal<Principal>();

    @Override
    public Principal getUser()
    {
        return threadLocal.get();
    }

    @Override
    public void setUser(final Principal user)
    {
        threadLocal.set(user);
    }

    @Override
    public void clearUser()
    {
        setUser(null);
    }
}
