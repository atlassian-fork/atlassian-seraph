package com.atlassian.seraph.auth;

/**
 * Enumeration used to determine the cause of any authentication errors.
 * <p>
 * Currently this is just used to indicate if a remote communication error has occurred (eg with LDAP server).
 */
public enum AuthenticationErrorType
{
    CommunicationError, UnknownError
}
