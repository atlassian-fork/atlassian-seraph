package com.atlassian.seraph.auth;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that this authenticator stores the user in the AuthenticationContext and does not rely
 * on the old behavior of com.atlassian.seraph.filter.BaseLoginFilter.SecurityHttpRequestWrapper.getRemoteUser()
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AuthenticationContextAwareAuthenticator { }
