package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;

import java.security.Principal;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * Provides a simple base implementation of RoleMapper.
 * <p>
 * It takes no initialisation parameters, and assumes all users that exist can log in.
 * <p>
 * This abstract class is a replacement for people that were previously extending the "GroupRoleMapper".
 *
 * @since v2.4.0
 */
public abstract class SimpleAbstractRoleMapper implements RoleMapper
{
    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        // No-op
    }

    /**
     * In the SimpleAbstractRoleMapper implementation, Users can login if they exist.
     */
    @Override
    public boolean canLogin(final Principal user, final HttpServletRequest request)
    {
        return user != null;
    }
}
