package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.elevatedsecurity.ElevatedSecurityGuard;
import com.atlassian.seraph.interceptor.LogoutInterceptor;
import com.atlassian.seraph.service.rememberme.RememberMeService;
import com.atlassian.seraph.util.RedirectUtils;
import com.atlassian.seraph.util.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.Principal;
import java.util.List;
import java.util.Map;

import static com.atlassian.seraph.auth.LoginReason.AUTHENTICATED_FAILED;
import static com.atlassian.seraph.auth.LoginReason.AUTHENTICATION_DENIED;
import static com.atlassian.seraph.auth.LoginReason.AUTHORISATION_FAILED;
import static com.atlassian.seraph.auth.LoginReason.OK;
import static com.atlassian.seraph.auth.LoginReason.OUT;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * This authenticator stores the currently logged in user in the session as a Principal.
 * <p>
 * It also provides for cookie logins and creates cookies if needed.
 * <p>
 * Includes code from Jive 1.2.4 (released under the Apache license)
 */
public abstract class DefaultAuthenticator extends AbstractAuthenticator
{
    /**
     * The key used to store the user object in the session
     */
    public static final String LOGGED_IN_KEY = "seraph_defaultauthenticator_user";

    /**
     * The key used to indicate that the user has logged out and session regarding of it containing a cookie is not
     * logged in.
     */
    public static final String LOGGED_OUT_KEY = "seraph_defaultauthenticator_logged_out_user";

    private static final Logger log = LoggerFactory.getLogger(DefaultAuthenticator.class);

    // --------------------------------------------------------------------------------------------------------- members

    private String basicAuthParameterName;

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        super.init(params, config);
        basicAuthParameterName = config.getAuthType();
    }

    /**
     * Tries to authenticate a user.
     *
     * @param httpServletRequest  the request in play
     * @param httpServletResponse the response in play
     * @param userName            the user name to check against the password
     * @param password            the password to authenticate the user with
     * @param setRememberMeCookie whether to set a remember me cookie on sucessful login
     * @return Whether the user was authenticated. This base implementation returns false if any errors occur, rather
     *         than throw an exception.
     * @throws AuthenticatorException actualy this class does not throw any exceptions however the interface says we
     *                                must and other classes may override us
     */
    @Override
    public boolean login(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final String userName, final String password, final boolean setRememberMeCookie)
            throws AuthenticatorException
    {
        final String METHOD = "login : ";
        final boolean dbg = log.isDebugEnabled();

        final Principal principal = new Principal()
        {
            @Override
            public String getName()
            {
                return userName;
            }
        };

        final boolean authenticated = authenticate(principal, password);
        if (dbg)
        {
            log.debug(METHOD + "'" + userName + "' has " + (authenticated ? "been" : "not been") + " authenticated");
        }
        if (authenticated)
        {
            final Principal user = getUser(userName);
            if (authoriseUserAndEstablishSession(httpServletRequest, httpServletResponse, user))
            {
                if (setRememberMeCookie && httpServletResponse != null)
                {
                    getRememberMeService().addRememberMeCookie(httpServletRequest, httpServletResponse, userName);
                }
                return true;
            }
            AUTHORISATION_FAILED.stampRequestResponse(httpServletRequest, httpServletResponse);
        }
        else
        {
            log.info(METHOD + "'" + userName + "' could not be authenticated with the given password");
        }

        if ((httpServletResponse != null))
        {
            log.warn(METHOD + "'" + userName + "' tried to login but they do not have USE permission or weren't found. Deleting remember me cookie.");

            getRememberMeService().removeRememberMeCookie(httpServletRequest, httpServletResponse);
        }

        return false;
    }

    /**
     * Called to remove the current principal from the HttpSession and will also to remove any remember me cookies that
     * may be in effect.
     *
     * @param httpServletRequest  the request in play
     * @param httpServletResponse the response in play
     * @return true always for this implementation!
     * @throws AuthenticatorException this implementation never does
     */
    @Override
    public boolean logout(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws AuthenticatorException
    {
        final String METHOD = "logout : ";
        final boolean dbg = log.isDebugEnabled();
        if (dbg)
        {
            log.debug(METHOD + "Calling interceptors and clearing remember me cookie");
        }
        final List<LogoutInterceptor> interceptors = getLogoutInterceptors();

        for (final LogoutInterceptor interceptor : interceptors)
        {
            interceptor.beforeLogout(httpServletRequest, httpServletResponse);
        }

        removePrincipalFromSessionContext(httpServletRequest);

        OUT.stampRequestResponse(httpServletRequest, httpServletResponse);

        // Log out is sometimes called as part of a getUser request, if the user is not found
        // log out may be called, but some getUser calls only pass in the request, and null response.
        if (httpServletResponse != null)
        {
            getRememberMeService().removeRememberMeCookie(httpServletRequest, httpServletResponse);
        }

        for (final Object element : interceptors)
        {
            final LogoutInterceptor interceptor = (LogoutInterceptor) element;
            interceptor.afterLogout(httpServletRequest, httpServletResponse);
        }

        return true;
    }

    /**
     * This is called to authorise the user with the application.  The {@link RoleMapper} is invoked to see if the user
     * is authorised to user this request  via a call to {@link #isAuthorised(javax.servlet.http.HttpServletRequest,
     * java.security.Principal)}
     * <p>
     * If successful, then the HttpSession will contain the attribute marking that the user is logged in
     *
     * @param httpServletRequest  the request in play
     * @param httpServletResponse the response in play
     * @param principal           the principal to authorise
     * @return true if the user was authorised
     */
    protected boolean authoriseUserAndEstablishSession(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse, final Principal principal)
    {
        //
        // Are they in fact already in the session?  if someone is using Basic Auth then this will be called for every request
        // and we don't want to tear down the session for every request
        //

        final boolean principalAlreadyInSessionContext = isPrincipalAlreadyInSessionContext(httpServletRequest, principal);

        // start clean at this point
        putPrincipalInSessionContext(httpServletRequest, null);

        final boolean canLogin = isAuthorised(httpServletRequest, principal);

        if (log.isDebugEnabled())
        {
            final String prefix = "authoriseUser : " + "'" + principal.getName() + "' ";
            log.debug(prefix + (canLogin ? "can" : "CANNOT") + " login according to the RoleMapper");
        }
        if (canLogin)
        {
            if (!principalAlreadyInSessionContext)
            {
                final SecurityConfig theConfig = getConfig();
                if (theConfig != null && theConfig.isInvalidateSessionOnLogin())
                {
                    invalidateSession(httpServletRequest);
                }
            }
            putPrincipalInSessionContext(httpServletRequest, principal);
            return true;
        }
        return false;
    }

    /**
     * This method is called to estblish if the principal  is authorised to use the appliction url in play
     *
     * @param httpServletRequest the request in play
     * @param principal          the principal to check
     * @return true if they are authorised to use the application at thgis point of time
     */
    protected boolean isAuthorised(final HttpServletRequest httpServletRequest, final Principal principal)
    {
        return getRoleMapper().canLogin(principal, httpServletRequest);
    }

    /**
     * This can be called to put the principal into the HttpSession in a Seraph ready manner
     *
     * @param httpServletRequest the request in play
     * @param principal          the principal to put in the session
     */
    protected void putPrincipalInSessionContext(final HttpServletRequest httpServletRequest, final Principal principal)
    {
        final HttpSession httpSession = httpServletRequest.getSession();
        httpSession.setAttribute(LOGGED_IN_KEY, principal);
        httpSession.setAttribute(LOGGED_OUT_KEY, null);
    }

    /**
     * This can be called to remove the principal into the HttpSession in a Seraph ready manner
     *
     * @param httpServletRequest the request in play
     */
    protected void removePrincipalFromSessionContext(final HttpServletRequest httpServletRequest)
    {
        final HttpSession httpSession = httpServletRequest.getSession();
        httpSession.setAttribute(LOGGED_IN_KEY, null);
        httpSession.setAttribute(LOGGED_OUT_KEY, Boolean.TRUE);
    }

    /**
     * This is called to determine if the Principal is already in the HttpSession in a Seraph ready manner.
     *
     * @param httpServletRequest the request in play
     * @param principal          the principal to put in the session
     * @return true if the principal is already in the session
     */
    protected boolean isPrincipalAlreadyInSessionContext(final HttpServletRequest httpServletRequest, final Principal principal)
    {
        Principal currentPrincipal = (Principal) httpServletRequest.getSession().getAttribute(LOGGED_IN_KEY);
        return currentPrincipal != null && currentPrincipal.getName() != null && principal != null && currentPrincipal.getName().equals(principal.getName());
    }

    /**
     * override this method if you need to retrieve the role mapper from elsewhere than the singleton-factory (injected
     * dependency for instance)
     *
     * @return the {@link com.atlassian.seraph.auth.RoleMapper} to use
     */
    protected RoleMapper getRoleMapper()
    {
        return SecurityConfigFactory.getInstance().getRoleMapper();
    }

    /**
     * Retrieve a Principal for the given username. Returns null if no such user exists.
     *
     * @param username the name of the user to find
     * @return a Principal for the given username.
     */
    protected abstract Principal getUser(final String username);

    /**
     * Authenticates the given user and password. Returns <tt>true</tt> if the authentication succeeds,
     * and <tt>false</tt> if the authentication details are invalid or if the user is not found.
     * Implementations of this method must not attempt to downcast the user to an implementation class.
     *
     * @param user     the user to authenticate. This object only stores the username of the user.
     * @param password the password of the user
     * @return true if the user was successfully authenticated and false otherwise.
     * @throws AuthenticatorException if an error occurs that stops the user from being authenticated (eg remote communication failure).
     */
    protected abstract boolean authenticate(final Principal user, final String password) throws AuthenticatorException;

    /**
     * Returns the currently logged in user, trying in order: <p> <ol> <li>Session, only if one exists</li> <li>Cookie,
     * only if no session exists</li> <li>Basic authentication, if the above fail, and authType=basic</li> </ol> <p>
     * Warning: only in the case of cookie and basic auth will the user be authenticated.
     *
     * @param httpServletRequest  the request in play
     * @param httpServletResponse a response object that may be modified if basic auth is enabled
     * @return a Principal object for the user if found, otherwise null
     */
    @Override
    public Principal getUser(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
    {
        final String METHOD = "getUser : ";
        final boolean dbg = log.isDebugEnabled();

        // Rules
        // - Do not create a session if there is not one already
        // - If there is a user in the session, return it
        // - Do not use a remember me cookie if this request was a logout request (e.g. make no assumption about the existence of a session or the existence of a user in that session - see SER-110, SER-114 and SER-95)
        // - If this is NOT a logout request, feel free to check the remember me cookie

        final HttpSession session = httpServletRequest.getSession(false);
        if (session != null)
        {
            final Principal sessionUser = getUserFromSession(httpServletRequest);
            if (sessionUser != null)
            {
                OK.stampRequestResponse(httpServletRequest, httpServletResponse);
                return sessionUser;
            }
        }

        // Look for remember me cookie, unless the user has logged out on this request (see Rules comment above)
        if (!OUT.isStamped(httpServletRequest))
        {
            final Principal cookieUser = getUserFromCookie(httpServletRequest, httpServletResponse);
            if (cookieUser != null)
            {
                return cookieUser;
            }
        }

        if (RedirectUtils.isBasicAuthentication(httpServletRequest, basicAuthParameterName))
        {
            final Principal basicAuthUser = getUserFromBasicAuthentication(httpServletRequest, httpServletResponse);
            if (basicAuthUser != null)
            {
                return basicAuthUser;
            }
        }

        if (dbg)
        {
            log.debug(METHOD + "User not found in either Session, Cookie or Basic Auth.");
        }

        return null;
    }

    /**
     * This is called to refresh the Principal object that has been retreived from the HTTP session.
     * <p>
     * By default this will called {@link #getUser(String)} again to get a fresh user.
     *
     * @param httpServletRequest the HTTP request in play
     * @param principal          the Principal in play
     * @return a fresh up to date principal
     */
    protected Principal refreshPrincipalObtainedFromSession(HttpServletRequest httpServletRequest, Principal principal)
    {
        Principal freshPrincipal = principal;
        if (principal != null && principal.getName() != null)
        {
            freshPrincipal = getUser(principal.getName());
            putPrincipalInSessionContext(httpServletRequest, freshPrincipal);
        }
        return freshPrincipal;
    }

    /**
     * <p> Tries to get a logged in user from the session. </p>
     *
     * @param httpServletRequest the current {@link HttpServletRequest}
     * @return the logged in user in the session. <code>null</code> if there is no logged in user in the session, or the
     *         {@link #LOGGED_OUT_KEY} is set because the user has logged out.
     */
    protected Principal getUserFromSession(final HttpServletRequest httpServletRequest)
    {
        final String METHOD = "getUserFromSession : ";
        final boolean dbg = log.isDebugEnabled();
        try
        {
            if (httpServletRequest.getSession().getAttribute(LOGGED_OUT_KEY) != null)
            {
                if (dbg)
                {
                    log.debug(METHOD + "Session found; user has already logged out. eg has LOGGED_OUT_KEY in session");
                }
                return null;
            }
            final Principal principal = (Principal) httpServletRequest.getSession().getAttribute(LOGGED_IN_KEY);
            if (dbg)
            {
                if (principal == null)
                {
                    log.debug(METHOD + "Session found; BUT it has no Principal in it");
                }
                else
                {
                    log.debug(METHOD + "Session found; '" + principal.getName() + "' is present");
                }
            }
            return refreshPrincipalObtainedFromSession(httpServletRequest, principal);
        }
        catch (final Exception e)
        {
            log.warn(METHOD + "Exception when retrieving user from session: " + e, e);
            return null;
        }
    }

    /**
     * Extracts the username and password from the cookie and calls login to authenticate, and if successful store the
     * token in the session.
     *
     * @param httpServletRequest  the HTTP request in play
     * @param httpServletResponse the HTTP respone in play
     * @return a Principal object for the user if successful, otherwise null
     */
    protected Principal getUserFromCookie(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
    {
        final String METHOD = "getUserFromCookie : ";
        final boolean dbg = log.isDebugEnabled();


        final String userName = getRememberMeService().getRememberMeCookieAuthenticatedUsername(httpServletRequest, httpServletResponse);
        if (dbg)
        {
            log.debug(METHOD + "Got username : '" + userName + "' from cookie, attempting to authenticate user is known");
        }
        if (isNotBlank(userName))
        {
            //
            // we have a valid user name so we need to turn them into a principal
            //
            final Principal principal = getUser(userName);
            if (principal != null)
            {
                // we have a valid user but is their account in a valid state from a security point of view
                final ElevatedSecurityGuard securityGuard = getElevatedSecurityGuard();
                if (!securityGuard.performElevatedSecurityCheck(httpServletRequest, userName))
                {
                    if (dbg)
                    {
                        log.debug(METHOD + "'" + userName + "' failed elevated security check");
                    }
                    AUTHENTICATION_DENIED.stampRequestResponse(httpServletRequest, httpServletResponse);
                    securityGuard.onFailedLoginAttempt(httpServletRequest, userName);
                    return null;
                }
                //
                // ok we have a valid Principal but is he allowed to login to the application at this stage
                // once we come out of this method, the user will be in the session and hence the "login session"
                // will have been established.
                //
                // In the past this was done all in the login() method but since don't login
                // we need to recall this authorisation step here
                //
                if (authoriseUserAndEstablishSession(httpServletRequest, httpServletResponse, principal))
                {
                    if (dbg)
                    {
                        log.debug(METHOD + "Authenticated '" + userName + "' via Remember Me Cookie");
                    }
                    OK.stampRequestResponse(httpServletRequest, httpServletResponse);
                    securityGuard.onSuccessfulLoginAttempt(httpServletRequest, userName);
                    return principal;
                }
                if (dbg)
                {
                    log.debug(METHOD + "'" + userName + "' failed authorisation security check");
                }
                AUTHORISATION_FAILED.stampRequestResponse(httpServletRequest, httpServletResponse);
                securityGuard.onFailedLoginAttempt(httpServletRequest, userName);
            }
        }
        return null;
    }

    /**
     * Checks the Authorization header to see whether basic auth token is provided. If it is, decode it, login and
     * return the valid user. If it isn't, basic auth is still required, so return a 401 Authorization Required header
     * in the response.
     *
     * @param httpServletRequest  the HTTP request in play
     * @param httpServletResponse a response object that <i>will</i> be modified if no token found
     * @return a {@link java.security.Principal} or null if one cant be found
     */
    protected Principal getUserFromBasicAuthentication(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
    {
        final String METHOD = "getUserFromSession : ";
        final boolean dbg = log.isDebugEnabled();

        final String header = httpServletRequest.getHeader("Authorization");
        LoginReason reason = OK;

        if (SecurityUtils.isBasicAuthorizationHeader(header))
        {
            if (dbg)
            {
                log.debug(METHOD + "Looking in Basic Auth headers");
            }

            final SecurityUtils.UserPassCredentials creds = SecurityUtils.decodeBasicAuthorizationCredentials(header);
            final ElevatedSecurityGuard securityGuard = getElevatedSecurityGuard();
            if (!securityGuard.performElevatedSecurityCheck(httpServletRequest, creds.getUsername()))
            {
                if (dbg)
                {
                    log.debug(METHOD + "'" + creds.getUsername() + "' failed elevated security check");
                }
                reason = AUTHENTICATION_DENIED.stampRequestResponse(httpServletRequest, httpServletResponse);
                securityGuard.onFailedLoginAttempt(httpServletRequest, creds.getUsername());
            }
            else
            {
                if (dbg)
                {
                    log.debug(METHOD + "'" + creds.getUsername() + "' does not require elevated security check.  Attempting authentication...");
                }

                try
                {
                    final boolean loggedin = login(httpServletRequest, httpServletResponse, creds.getUsername(),
                            creds.getPassword(), false);
                    if (loggedin)
                    {
                        reason = OK.stampRequestResponse(httpServletRequest, httpServletResponse);
                        securityGuard.onSuccessfulLoginAttempt(httpServletRequest, creds.getUsername());
                        if (dbg)
                        {
                            log.debug(METHOD + "Authenticated '" + creds.getUsername() + "' via Basic Auth");
                        }
                        return getUser(creds.getUsername());
                    }
                    else
                    {
                        reason = AUTHENTICATED_FAILED.stampRequestResponse(httpServletRequest, httpServletResponse);
                        securityGuard.onFailedLoginAttempt(httpServletRequest, creds.getUsername());
                    }
                }
                catch (final AuthenticatorException e)
                {
                    log.warn(METHOD + "Exception trying to login '" + creds.getUsername() + "' via Basic Auth:" + e, e);
                }
            }
            if (httpServletResponse != null)
            {
                try
                {
                    httpServletResponse.sendError(401, "Basic Authentication Failure - Reason : " + reason.toString());
                }
                catch (final IOException e)
                {
                    log.warn(METHOD + "Exception trying to send Basic Auth failed error: " + e, e);
                }
            }
            return null;
        }

        if (httpServletResponse != null)
        {
            httpServletResponse.setStatus(401);
            httpServletResponse.setHeader("WWW-Authenticate", "Basic realm=\"protected-area\"");
        }
        return null;
    }

    public String getAuthType()
    {
        return basicAuthParameterName;
    }

    protected List<LogoutInterceptor> getLogoutInterceptors()
    {
        return getConfig().getInterceptors(LogoutInterceptor.class);
    }

    protected ElevatedSecurityGuard getElevatedSecurityGuard()
    {
        return getConfig().getElevatedSecurityGuard();
    }

    protected RememberMeService getRememberMeService()
    {
        return getConfig().getRememberMeService();
    }

    private void invalidateSession(HttpServletRequest httpServletRequest)
    {
        SessionInvalidator si = new SessionInvalidator(getConfig().getInvalidateSessionExcludeList());
        si.invalidateSession(httpServletRequest);
    }

}
