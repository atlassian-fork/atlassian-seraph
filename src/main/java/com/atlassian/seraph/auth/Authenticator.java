package com.atlassian.seraph.auth;

import com.atlassian.seraph.Initable;
import com.atlassian.seraph.service.rememberme.RememberMeToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * An Authenticator is used to authenticate users, log them in, log them out and check their roles.
 */
public interface Authenticator extends Initable
{
    public void destroy();

    /**
     * Gets the username of the {@link Principal} authenticated for the given {@link HttpServletRequest}.
     * @param request
     * @return username or null if no user has been authenticated
     */
    public String getRemoteUser(HttpServletRequest request);

     /**
     * Gets the {@link Principal} authenticated for the given {@link HttpServletRequest}.
     * @param request
     * @return principal or null if no user has been authenticated
     */
    public Principal getUser(HttpServletRequest request);

    /**
     * Gets the {@link Principal} for the {@link HttpServletRequest}. The {@link RememberMeToken} will be regenerated for the {@link HttpServletResponse} if the token is invalid.
     * @param request
     * @param response
     * @return principal or null if no user has been authenticated
     */
    public Principal getUser(HttpServletRequest request, HttpServletResponse response);

    /**
     * Tries to authenticate a user.
     *
     * @param request             the HttpServletRequest
     * @param response            the HttpServletResponse
     * @param username            the user name to check against the password
     * @param password            the password to authenticate the user with
     * @return Whether the user was authenticated. This should only return false if we were able to actually test and fail the login attempt.
     *
     * @throws AuthenticatorException if an error occurs that stops the user from being authenticated (eg remote communication failure).
     */
    public boolean login(HttpServletRequest request, HttpServletResponse response, String username, String password) throws AuthenticatorException;

    /**
     * Tries to authenticate a user.
     *
     * @param request             the HttpServletRequest
     * @param response            the HttpServletResponse
     * @param username            the user name to check against the password
     * @param password            the password to authenticate the user with
     * @param storeCookie         whether to set a remember me cookie on successful login
     * @return Whether the user was authenticated. This should only return false if we were able to actually test and fail the login attempt.
     *
     * @throws AuthenticatorException if an error occurs that stops the user from being authenticated (eg remote communication failure).
     */
    public boolean login(HttpServletRequest request, HttpServletResponse response, String username, String password, boolean storeCookie) throws AuthenticatorException;

    public boolean logout(HttpServletRequest request, HttpServletResponse response) throws AuthenticatorException;
}
