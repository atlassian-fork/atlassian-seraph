package com.atlassian.seraph.auth;

import com.atlassian.seraph.config.SecurityConfig;

import java.io.Serializable;
import java.security.Principal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * An abstract implementation of Authenticator that implements a lot of base methods
 */
public abstract class AbstractAuthenticator implements Authenticator, Serializable
{
    private SecurityConfig config;

    @Override
    public void init(final Map<String, String> params, final SecurityConfig config)
    {
        this.config = config;
    }

    @Override
    public void destroy()
    {}

    @Override
    public String getRemoteUser(final HttpServletRequest request)
    {
        final Principal user = getUser(request);

        if (user == null)
        {
            return null;
        }

        return user.getName();
    }

    @Override
    public Principal getUser(final HttpServletRequest request)
    {
        return getUser(request, null);
    }

    @Override
    public abstract Principal getUser(HttpServletRequest request, HttpServletResponse response);

    @Override
    public boolean login(final HttpServletRequest request, final HttpServletResponse response, final String username, final String password) throws AuthenticatorException
    {
        return login(request, response, username, password, false);

    }

    @Override
    public abstract boolean login(HttpServletRequest request, HttpServletResponse response, String username, String password, boolean cookie) throws AuthenticatorException;

    @Override
    public abstract boolean logout(HttpServletRequest request, HttpServletResponse response) throws AuthenticatorException;

    protected SecurityConfig getConfig()
    {
        return config;
    }
}
